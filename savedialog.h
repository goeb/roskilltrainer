/*

Copyright 2011, 2014 Stefan Goebel <roskilltrainer -at- subtype.de> - Homepage: <http://subtype.de/>

This file is part of ROSkillTrainer.

ROSkillTrainer is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
version.

ROSkillTrainer is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with ROSkillTrainer. If not, see
<http://www.gnu.org/licenses/>.

*/

#ifndef SAVEDIALOG_H
#define SAVEDIALOG_H

#include "trainerconfig.h"
#include "trainersetups.h"

#include <QDialog>

namespace Ui {
    class SaveDialog;
}

class SaveDialog: public QDialog {

    Q_OBJECT

    public:

        explicit SaveDialog (QWidget *parent = 0);
        ~SaveDialog ();

        QString getLastName () const;
        QString getLastDescription () const;

    public slots:

        void reject ();

    private:

        QString name, description;
        TrainerSetups *ts;
        TrainerConfig *tc;
        Ui::SaveDialog *ui;

    private slots:

        void onNameEdited (QString newName);
        void save ();

};

#endif // SAVEDIALOG_H
