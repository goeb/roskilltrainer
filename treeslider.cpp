/*

Copyright 2011, 2014 Stefan Goebel <roskilltrainer -at- subtype.de> - Homepage: <http://subtype.de/>

This file is part of ROSkillTrainer.

ROSkillTrainer is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
version.

ROSkillTrainer is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with ROSkillTrainer. If not, see
<http://www.gnu.org/licenses/>.

*/

#include "treeslider.h"

TreeSlider::TreeSlider (QWidget *parent): LevelSlider (parent) {

    hide ();

    tc = TrainerConfig::config ();
    pp = PointsPool::pool ();

    connect (pp, SIGNAL (dpAvailableChanged (int)), SLOT (updateMaximum (int)));

    setFixedSize (39, 39);
    setTextStyle (17, 18, 20, 14, "background-color: #000; color: #fff; font-weight: bold;");

    setMinimum (1);
    setMaximum (1);

    connect (this, SIGNAL (valueChanged (int)), SLOT (changePoints (int)));

    origLevel = 1;
    updateMaximum (pp -> getDPAvailable ());

    setEnabled (false);

}

void TreeSlider::setTree (QString icon, QString tooltip) {
    setIcon (icon);
    setToolTip (tooltip);
    setEnabled (true);
    show ();
}

void TreeSlider::updateMaximum (int avail) {
    if (avail < tc -> dpCost (value ())) setTempCap (-1);
    else releaseTempCap ();
}

void TreeSlider::changePoints (int newLevel) {
    if (origLevel != newLevel) {
        pp -> incDPUsed (tc -> dpCost (origLevel, newLevel));
        origLevel = newLevel;
    }
}

void TreeSlider::reset () {
    hide ();
    LevelSlider::reset ();
    setMaximum (1);
    setToolTip ("");
    origLevel = 1;
    updateMaximum (pp -> getDPAvailable ());
    setEnabled (false);
}
