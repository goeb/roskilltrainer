/*

Copyright 2011, 2014 Stefan Goebel <roskilltrainer -at- subtype.de> - Homepage: <http://subtype.de/>

This file is part of ROSkillTrainer.

ROSkillTrainer is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
version.

ROSkillTrainer is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with ROSkillTrainer. If not, see
<http://www.gnu.org/licenses/>.

*/

// base class for the treeslider and spellslider classes, provides a widget with an icon and level label, and two
// buttons to increase/decrease the level (visible on mouseover, can be disabled). the icon will be greyed out if the
// level is 0. everything else is left to the QAbstractSlider class...

#include "levelslider.h"

#include <QEvent>
#include <QIcon>
#include <QPixmap>

LevelSlider::LevelSlider (QWidget *parent): QAbstractSlider (parent) {

    // temporary level cap indicator, any other value than -1 means the temporary cap is in effect, in that case tempCap
    // stores the original maximum value (maybe this should be renamed...)
    tempCap = -1;

    // default is to enable the buttons:
    buttonsEnabled = true;

    // gui stuff and graphics effect to grey out the icon when the level is 0.
    lbIcon = new QLabel (this);
    lbText = new QLabel (this);
    effect = new QGraphicsColorizeEffect (this);

    effect -> setColor (QColor (128, 128, 128));
    lbIcon -> setPixmap (0);
    lbIcon -> setGraphicsEffect (effect);
    lbText -> setAlignment (Qt::AlignCenter | Qt::AlignHCenter);

    // increase button. note that the button geometry is currently fixed.

    QIcon qiInc = QIcon ();
    qiInc.addFile (":/gui/inc_enabled.png",  QSize (12, 12));
    qiInc.addFile (":/gui/inc_disabled.png", QSize (12, 12), QIcon::Disabled);

    pbInc = new QPushButton (this);
    pbInc -> hide ();
    pbInc -> setGeometry (27, 0, 12, 12);
    pbInc -> setFlat (true);
    pbInc -> setFocusPolicy (Qt::NoFocus);
    pbInc -> setIcon (qiInc);
    pbInc -> setStyleSheet ("background-color: transparent; border: none;");

    // decrease button:

    QIcon qiDec = QIcon ();
    qiDec.addFile (":/gui/dec_enabled.png",  QSize (12, 12));
    qiDec.addFile (":/gui/dec_disabled.png", QSize (12, 12), QIcon::Disabled);

    pbDec = new QPushButton (this);
    pbDec -> hide ();
    pbDec -> setGeometry (27, 12, 12, 12);
    pbDec -> setFlat (true);
    pbDec -> setFocusPolicy (Qt::NoFocus);
    pbDec -> setIcon (qiDec);
    pbDec -> setStyleSheet ("background-color: transparent; border: none;");

    // button actions:
    connect (pbInc, SIGNAL (clicked ()), SLOT (increaseLevel ()));
    connect (pbDec, SIGNAL (clicked ()), SLOT (decreaseLevel ()));

    // both single and page step will be 1. no idea which value a mouse wheel event will use. keyboard stuff does not
    // matter anyway since the widget can't get the focus...
    setSingleStep (1);
    setPageStep (1);

    // update the buttons and level label on every valueChanged event:
    connect (this, SIGNAL (valueChanged (int)), SLOT (updateDisplay (int)));

    updateDisplay (value ());

}

void LevelSlider::updateDisplay (int value) {

    // update the level label:
    lbText -> setNum (value);

    // if the level is 0, grey out the icon:
    if (value == 0) effect -> setEnabled (true);
    else effect -> setEnabled (false);

    // and enable/disable the increase/decrease buttons:
    pbInc -> setEnabled (value != maximum ());
    pbDec -> setEnabled (value != minimum ());

}

void LevelSlider::setIcon (QString file, int x, int y, int w, int h, bool scaled) {
    lbIcon -> setPixmap (QPixmap (file));
    lbIcon -> setGeometry (x, y, w, h);
    lbIcon -> setScaledContents (scaled);
}

void LevelSlider::setTextStyle (int x, int y, int w, int h, QString style) {
    lbText -> setGeometry (x, y, w, h);
    lbText -> setStyleSheet (style);
}

void LevelSlider::setTextStyleSheet (QString style) {
    lbText -> setStyleSheet (style);
}

void LevelSlider::reset () {
    // reset the widget. the value will be set to the minimum value, the pixmap will be removed and a temporary level
    // cap will be released. minimum and maximum values will be left as they are.
    setValue (minimum ());
    lbIcon -> setPixmap (0);
    tempCap = -1;
    updateDisplay (0);
}

void LevelSlider::setEnabled (bool enabled) {
    // note that disabling the widget will reset the level to the minimum value.
    if (enabled) lbText -> show ();
    else {
        setValue (minimum ());
        lbText -> hide ();
    }
    QAbstractSlider::setEnabled (enabled);
}

void LevelSlider::enterEvent (QEvent *event) {
    if (isEnabled () && buttonsEnabled) {
        pbInc -> show ();
        pbDec -> show ();
        event -> accept ();
    }
}

void LevelSlider::leaveEvent (QEvent *event) {
    pbInc -> hide ();
    pbDec -> hide ();
    event -> accept ();
}

void LevelSlider::increaseLevel () { setValue (value () + 1); }
void LevelSlider::decreaseLevel () { setValue (value () - 1); }

void LevelSlider::setMaximum (int max) {
    // sets a new maximum level. if the temporary level cap is active and the new maximum is higher than the cap, the
    // cap will stay in effect until released, if the new maximum is below the temporary cap, the maximum will be set
    // and the level cap will be released.
    if (tempCap != -1) {
        if (maximum () < max) {
            tempCap = max;
            return;
        }
        else tempCap = -1;
    }
    QAbstractSlider::setMaximum (max);
    updateDisplay (value ());
}

void LevelSlider::setMaximumTemp (int max) {
    // private helper function to be used by the tempCap functions...
    QAbstractSlider::setMaximum (max);
    updateDisplay (value ());
}

void LevelSlider::setMinimum (int min) {
    // note that the minimum must be greater than zero. (we do not restrict the maximum, QAbstractSlider should do most
    // of the work anyway...)
    if (min < 0) return;
    QAbstractSlider::setMinimum (min);
    updateDisplay (value ());
}

void LevelSlider::setTempCap (int level) {
    // sets a temporary level cap (temporary maximum value) of the widget. this maximum value stays in effect until
    // either releaseTempCap is called or a new maximum is set that is below the temporary level cap (using setMaximum).
    // if there is a level cap set already this function will call releaseTempCap first. call releaseTempCap to set the
    // maximum value to the original one. note that if the level parameter is -1 the cap will be set to the current
    // value.
    if (tempCap != -1) releaseTempCap ();
    tempCap = maximum ();
    setMaximumTemp (level == -1 ? value () : level);
    updateDisplay (value ());
}

void LevelSlider::releaseTempCap () {
    // release the temporary level cap and set the maximum level to the actual one.
    if (tempCap != -1) {
        setMaximumTemp (tempCap);
        tempCap = -1;
        updateDisplay (value ());
    }
}

void LevelSlider::setButtonsEnabled (bool enabled) {
    // if <enabled> is true, buttons will be displayed on mouseover, else this will be disabled.
    buttonsEnabled = enabled;
}
