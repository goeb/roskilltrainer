/*

Copyright 2011, 2014 Stefan Goebel <roskilltrainer -at- subtype.de> - Homepage: <http://subtype.de/>

This file is part of ROSkillTrainer.

ROSkillTrainer is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
version.

ROSkillTrainer is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with ROSkillTrainer. If not, see
<http://www.gnu.org/licenses/>.

*/

#include "importexportdialog.h"
#include "ui_importexportdialog.h"

#include <QClipboard>
#include <QDesktopServices>
#include <QMessageBox>
#include <QUrl>

ImportExportDialog::ImportExportDialog (QWidget *parent): QDialog (parent), ui (new Ui::ImportExportDialog) {

    lastCode = "";

    setWindowFlags (Qt::Dialog | Qt::CustomizeWindowHint | Qt::WindowTitleHint | Qt::WindowCloseButtonHint);

    ui -> setupUi (this);

    connect (ui -> pbCancel,    SIGNAL (clicked ()), SLOT (reject ()));
    connect (ui -> pbOK,        SIGNAL (clicked ()), SLOT (accept ()));
    connect (ui -> pbClipboard, SIGNAL (clicked ()), SLOT (copyToClipboard ()));
    connect (ui -> pbOpen,      SIGNAL (clicked ()), SLOT (openURL ()));

    connect (ui -> teURL, SIGNAL (textChanged ()), SLOT (checkURL ()));

}

ImportExportDialog::~ImportExportDialog () {
    delete ui;
}

int ImportExportDialog::exec (ImportExport type, QString code) {

    // parameters to exec decide whether the import or export funxtion is used. in case of the export function the
    // second parameter has to be set to the encoded trainer setup string.

    if (type == Import) {
        lastCode = "";
        ui -> lbText -> setText ("Enter the Clan Inquisition Trainer &URL to import:");
        ui -> pbClipboard -> hide ();
        ui -> pbOpen -> hide ();
        ui -> pbCancel -> show ();
        ui -> pbOK -> setEnabled (false);
        ui -> teURL -> setReadOnly(false);
        ui -> teURL -> clear ();
        ui -> lbWarning -> hide ();
    }
    else {
        ui -> lbText -> setText ("Clan Inquisition Trainer URL for the current setup:");
        ui -> pbClipboard -> show ();
        ui -> pbOpen -> show ();
        ui -> pbOK -> setEnabled (true);
        ui -> pbCancel -> hide ();
        ui -> teURL -> setReadOnly (true);
        ui -> teURL -> setPlainText (URL_PREFIX + code);
        ui -> lbWarning -> hide ();
    }

    return QDialog::exec ();

}

void ImportExportDialog::copyToClipboard () {
    // copies the text in the text edit to the clipboard.
    QClipboard *clipboard = QApplication::clipboard ();
    clipboard -> setText (ui -> teURL -> toPlainText ());
}

void ImportExportDialog::openURL () {
    // opens the url in the text edit in the default browser. a message box will be shown if this fails.
    if (! QDesktopServices::openUrl (QUrl (ui -> teURL -> toPlainText ())))
        QMessageBox::warning (
                this,
                "Regnum Online Skill Trainer",
                tr ("Could not open the default browser."),
                QMessageBox::Ok
            );
}

void ImportExportDialog::checkURL () {

    QString tCode;
    QString chars = TrainerCodec::getEncodeChars ();
    QString text  = ui -> teURL -> toPlainText ().remove (QRegExp ("^\\s*")).remove (QRegExp ("\\s*$"));

    // check if only a code is entered first:
    if (QRegExp ("^[" + chars + "]+$").exactMatch (text)) {
        checkCode (text);
    }

    // possibly valid url, strip the url from the code:
    else if (text.startsWith (URL_PREFIX)) {
        text.remove (URL_PREFIX);
        checkCode (text);
    }

    // everything else:
    else {
        ui -> lbWarning -> show ();
        ui -> pbOK -> setEnabled (false);
    }

}

void ImportExportDialog::checkCode (QString code) {
    // creates a TrainerCodec instance and checks the validity of the code. if it is valid, the ok button will be
    // enabled, else disabled.
    TrainerCodec *tc = new TrainerCodec (code);
    if (tc -> isValid ()) {
        ui -> lbWarning -> hide ();
        ui -> pbOK -> setEnabled (true);
        lastCode = code;
    }
    else {
        ui -> lbWarning -> show ();
        ui -> pbOK -> setEnabled (false);
        lastCode = "";
    }
}

QString ImportExportDialog::getCode () {
    // returns the last code to import, will be an empty string if there was no code or no valid code yet.
    return lastCode;
}
