/*

Copyright 2011, 2014 Stefan Goebel <roskilltrainer -at- subtype.de> - Homepage: <http://subtype.de/>

This file is part of ROSkillTrainer.

ROSkillTrainer is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
version.

ROSkillTrainer is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with ROSkillTrainer. If not, see
<http://www.gnu.org/licenses/>.

*/

#ifndef TRAINERCONFIG_H
#define TRAINERCONFIG_H

#include <QMap>
#include <QObject>
#include <QPair>
#include <QStringList>
#include <QVector>

class TrainerConfig: public QObject {

    Q_OBJECT

    public:

        static TrainerConfig* config (QObject *parent = 0);

        int dpCost (int from, int to = -1) const;
        int spellLevel (int level);
        int treeLevel (int level) const;
        int maxTreeLevel () const;

        QVector <QPair <QString, bool> > classList () const;
        QStringList classStringList (bool indent = true) const;
        int classIndex (QString className) const;
        bool classExists (QString className, Qt::CaseSensitivity cs = Qt::CaseSensitive) const;
        QString classNameClean (QString className) const;
        int numSubClasses (QString className) const;

        QPair <int, int> classLimits (QString className) const;
        QStringList classTrees (QString className) const;

        int classDP (QString className, int level) const;
        int classPP (QString className, int level) const;

        int minTreeNumber () const;
        int maxTreeNumber () const;
        int spellNumber () const;

    private:

        struct tClassInfo {
            QStringList spells;
            QVector <int> *disciplinePoints, *powerPoints;
            int minLevel, maxLevel;
            bool isSubClass;
        };

        struct tPointsInfo {
            QVector <int> *disciplinePoints, *powerPoints;
            int minLevel, maxLevel;
        };

        explicit TrainerConfig (QObject *parent);
        QVector <int> readSingleValues (QString fName);
        tPointsInfo readPointValues (QString fName);
        void loadClasses (QString fName);

        static TrainerConfig *instance;

        QStringList classOrder;
        QMap <QString, tClassInfo> classInfo;
        QVector <int> disciplineCosts, maxSpellLevels, maxTreeLevels;
        QMap <QString, QStringList> subClasses;

};

#endif // TRAINERCONFIG_H
