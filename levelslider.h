/*

Copyright 2011, 2014 Stefan Goebel <roskilltrainer -at- subtype.de> - Homepage: <http://subtype.de/>

This file is part of ROSkillTrainer.

ROSkillTrainer is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
version.

ROSkillTrainer is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with ROSkillTrainer. If not, see
<http://www.gnu.org/licenses/>.

*/

#ifndef LEVELSLIDER_H
#define LEVELSLIDER_H

#include <QAbstractSlider>
#include <QGraphicsColorizeEffect>
#include <QLabel>
#include <QPushButton>

class LevelSlider: public QAbstractSlider {

    Q_OBJECT

    public:

        explicit LevelSlider (QWidget *parent = 0);

        void setIcon (QString file, int x = 0, int y = 0, int w = 32, int h = 32, bool scaled = true);

        void setTextStyle (int x, int y, int w, int h, QString style = "");
        void setTextStyleSheet (QString style);

        void setEnabled (bool enabled);
        void setButtonsEnabled (bool enabled);
        void setMaximum (int max);
        void setMinimum (int min);

        void setTempCap (int level = -1);
        void releaseTempCap ();

        void reset ();

    public slots:

        void updateDisplay (int value);
        void increaseLevel ();
        void decreaseLevel ();

    private:

        QLabel *lbIcon, *lbText;
        QGraphicsColorizeEffect *effect;
        QPushButton *pbInc, *pbDec;
        int tempCap;
        bool buttonsEnabled;

        void setMaximumTemp (int max);

    protected:

        void enterEvent (QEvent *event);
        void leaveEvent (QEvent *event);

};

#endif // LEVELSLIDER_H
