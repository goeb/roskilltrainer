/*

Copyright 2011, 2014 Stefan Goebel <roskilltrainer -at- subtype.de> - Homepage: <http://subtype.de/>

This file is part of ROSkillTrainer.

ROSkillTrainer is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
version.

ROSkillTrainer is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with ROSkillTrainer. If not, see
<http://www.gnu.org/licenses/>.

*/

#include "resources.h"

#include <QFile>

Resources::Resources () {}

QString Resources::getIcon (QString fileName, QString defaultIcon) {
    // returns the <fileName> if the file exists and has non-zero size, else <defaultIcon>.
    QFile file (fileName);
    return (file.exists () && file.size () > 0 )? fileName : defaultIcon;
}

QString Resources::getText (QString fileName, QString defaultText) {
    // returns the contents of the file <fileName> if it exists, has non-zero size and can be opened, else
    // <defaultText>.
    QFile file (fileName);
    if (! (file.exists () && file.size () > 0 && file.open (QIODevice::ReadOnly | QIODevice::Text))) return defaultText;
    QString text = file.readAll ();
    file.close ();
    return text;
}
