#-------------------------------------------------
#
# Project created by QtCreator 2011-01-10T10:23:36
#
#-------------------------------------------------

QT       += core gui

TARGET = ROSkillTrainer
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    pointspool.cpp \
    loaddialog.cpp \
    levelslider.cpp \
    importexportdialog.cpp \
    treeslider.cpp \
    transparentslider.cpp \
    trainerwidget.cpp \
    trainersetups.cpp \
    trainergui.cpp \
    trainerconfig.cpp \
    trainercodec.cpp \
    spellslider.cpp \
    savedialog.cpp \
    treewidget.cpp \
    spellbar.cpp \
    resources.cpp

HEADERS  += mainwindow.h \
    pointspool.h \
    loaddialog.h \
    levelslider.h \
    importexportdialog.h \
    treeslider.h \
    transparentslider.h \
    trainerwidget.h \
    trainersetups.h \
    trainergui.h \
    trainerconfig.h \
    trainercodec.h \
    spellslider.h \
    savedialog.h \
    treewidget.h \
    spellbar.h \
    resources.h

FORMS    += mainwindow.ui \
    loaddialog.ui \
    importexportdialog.ui \
    savedialog.ui

OTHER_FILES +=

RESOURCES += \
    config.qrc \
    gui.qrc \
    spells.qrc
