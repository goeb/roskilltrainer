#!/usr/bin/perl

use warnings;
use strict;

use File::Fetch;
use LWP::UserAgent;

my $ua = LWP::UserAgent -> new ();

my $ua_string = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; pl; rv:1.9.1.3) Gecko/20090824 Firefox/3.5.3';

$ua -> agent ($ua_string);
$File::Fetch::USER_AGENT = $ua_string;

my $rs = $ua -> get ('http://regnum.wikia.com/wiki/Powers_Table');

if (! $rs -> is_success ()) {
	print $rs -> status_line, "\n";
	exit 1;
}

my @lines = split /[\r\n]+/, $rs -> decoded_content ();

my $dsc_old  = '';
my $num      = 0;
my $base_url = 'http://images.wikia.com/regnum/images';

while (@lines) {

	my $line = shift @lines;

	if ($line =~ m!"($base_url/[^"]+_Power_Icon\.(?:jpg|png))"!i) {

		my $pwr_ico = $1;
		my $pwr_ext = (split /\./, $pwr_ico) [-1];
		my $pwr_nam = (split /\//, $pwr_ico) [-1];

		shift @lines;
		my $next_line = shift @lines;

		if ($next_line =~ m!"($base_url/[^"]+_Discipline_Icon\.(?:jpg|png))"!i) {

			my $dsc_ico = $1;
			my $dsc_ext = (split /\./, $dsc_ico) [-1];
			my $dsc_nam = (split /\//, $dsc_ico) [-1];

			if ($next_line =~ m!>([^<]+)</a></span>!i) {

				my $dsc_dir = $1;
				$dsc_dir =~ tr/[A-Z ]/[a-z_]/;

				my $ff;

				if ($dsc_dir ne $dsc_old) {
					$dsc_old = $dsc_dir;
					$num = 0;
					unless (-f "$dsc_dir/icon.$dsc_ext" and -s "$dsc_dir/icon.$dsc_ext") {
						unlink ("$dsc_dir/icon.$dsc_ext") if (-f "$dsc_dir/icon.$dsc_ext");
						$ff = File::Fetch -> new ('uri' => $dsc_ico);
						mkdir $dsc_dir unless (-d $dsc_dir);
						chdir $dsc_dir;
						if ($ff -> fetch ()) {
							rename $dsc_nam, "icon.$dsc_ext";
							print "Got $dsc_dir/icon.$dsc_ext\n";
						}
						else {
							print "Error fetching $dsc_ico: " . $ff -> error ();
						}
						chdir '..';
					}
				}

				if (-d $dsc_dir) {
					chdir $dsc_dir;
					unless (-f "$num.$pwr_ext" and -s "$num.$pwr_ext") {
						unlink ("$num.$pwr_ext") if (-f "$num.$pwr_ext");
						$ff = File::Fetch -> new ('uri' => $pwr_ico);
						if ($ff -> fetch ()) {
							rename $pwr_nam, "$num.$pwr_ext";
							print "Got $dsc_dir/$num.$pwr_ext\n";
						}
						else {
							print "Error fetching $pwr_ico: " . $ff -> error ();
						}
					}
					chdir '..';
				}
				else {
					print "No directory created: $dsc_dir\n";
				}

			}

		}

		++ $num;

	}

}

