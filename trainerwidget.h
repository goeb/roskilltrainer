/*

Copyright 2011, 2014 Stefan Goebel <roskilltrainer -at- subtype.de> - Homepage: <http://subtype.de/>

This file is part of ROSkillTrainer.

ROSkillTrainer is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
version.

ROSkillTrainer is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with ROSkillTrainer. If not, see
<http://www.gnu.org/licenses/>.

*/

#ifndef TRAINERWIDGET_H
#define TRAINERWIDGET_H

#include "pointspool.h"
#include "trainerconfig.h"
#include "treewidget.h"

#include <QWidget>

class TrainerWidget: public QWidget {

    Q_OBJECT

    public:

        explicit TrainerWidget (QWidget *parent = 0);
        QString encoded (bool compat = false) const;
        bool decode (QString code);

    signals:

        void spellStatus (bool enabled, int tNum, int sNum);

    public slots:

        void setClass (QString newClass);
        void setLevel (int newLevel);
        void setLevel (QString newLevel);
        void resetPowers ();
        void showSliders (bool show);
        void showButtons (bool show);

    private:

        QVector <TreeWidget*> trees;
        TrainerConfig *tc;
        PointsPool *pp;
        QString currentClass;
        int currentLevel;

        QVector <int> getTreeLevels () const;
        QVector <QVector <int> > getSpellLevels () const;

    private slots:

        void emitSignals (bool enabled, int tNum, int sNum);

};

#endif // TRAINERWIDGET_H
