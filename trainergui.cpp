/*

Copyright 2011, 2014 Stefan Goebel <roskilltrainer -at- subtype.de> - Homepage: <http://subtype.de/>

This file is part of ROSkillTrainer.

ROSkillTrainer is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
version.

ROSkillTrainer is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with ROSkillTrainer. If not, see
<http://www.gnu.org/licenses/>.

*/

#include "trainergui.h"

#include <QApplication>
#include <QCoreApplication>
#include <QFileDialog>
#include <QHBoxLayout>
#include <QMenu>
#include <QMessageBox>
#include <QSettings>
#include <QVBoxLayout>

TrainerGUI::TrainerGUI (QWidget *parent): QFrame (parent) {

    tc = TrainerConfig::config (this);
    ts = TrainerSetups::setups (this);
    pp = PointsPool::pool (this);

    dgSave = new SaveDialog ();
    dgLoad = new LoadDialog ();
    dgImExport = new ImportExportDialog ();

    initialized = false;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // gui layout: /////////////////////////////////////////////////////////////////////////////////////////////////////

    setFixedSize (560, 705);

    QVBoxLayout *loMain = new QVBoxLayout (this);
    QHBoxLayout *loMenu = new QHBoxLayout ();
    QVBoxLayout *loGrab = new QVBoxLayout ();
    QHBoxLayout *loText = new QHBoxLayout ();

    // menu layout:

    lbClass = new QLabel (tr ("&Class:"), this);
    cbClass = new QComboBox (this);
    lbClass -> setBuddy (cbClass);
    cbClass -> addItem ("");
    cbClass -> addItems (tc -> classStringList ());

    lbLevel = new QLabel (tr ("Le&vel:"), this);
    cbLevel = new QComboBox (this);
    lbLevel -> setBuddy (cbLevel);
    lbLevel -> hide ();
    cbLevel -> hide ();

    pbReset = new QPushButton (tr ("&Reset"), this);
    pbReset -> hide ();

    pbMenu = new QPushButton (tr ("&Menu"), this);

    loMenu -> addWidget (lbClass);
    loMenu -> addWidget (cbClass);
    loMenu -> addSpacing (10);
    loMenu -> addWidget (lbLevel);
    loMenu -> addWidget (cbLevel);
    loMenu -> addSpacing (20);
    loMenu -> addWidget (pbReset);
    loMenu -> addStretch ();
    loMenu -> addWidget (pbMenu);

    // sub widget, used to save the setup as an image:

    wgGrab = new QWidget (this);
    wgGrab -> hide ();
    wgGrab -> setLayout (loGrab);

    lbDPAvail = new QLabel ("0", wgGrab);
    lbDPTotal = new QLabel ("0", wgGrab);
    lbPPAvail = new QLabel ("0", wgGrab);
    lbPPTotal = new QLabel ("0", wgGrab);

    lbPrintClass = new QLabel (wgGrab);
    lbPrintLevel = new QLabel (wgGrab);
    lbPrintClass -> hide ();
    lbPrintLevel -> hide ();
    lbPrintClass -> setStyleSheet ("font-weight: bold;");

    loText -> addWidget (new QLabel (tr ("Discipline Points:"), wgGrab));
    loText -> addWidget (lbDPAvail);
    loText -> addWidget (new QLabel (tr ("/"), wgGrab));
    loText -> addWidget (lbDPTotal);
    loText -> addSpacing (20);
    loText -> addWidget (new QLabel (tr ("Power Points:"), wgGrab));
    loText -> addWidget (lbPPAvail);
    loText -> addWidget (new QLabel (tr ("/"), wgGrab));
    loText -> addWidget (lbPPTotal);
    loText -> addStretch ();
    loText -> addWidget (lbPrintClass);
    loText -> addSpacing (10);
    loText -> addWidget (lbPrintLevel);

    trainer = new TrainerWidget (wgGrab);

    loGrab -> addLayout (loText);
    loGrab -> addWidget (trainer);

    // main layout:

    sb = new SpellBar (this);
    sb -> hide ();

    loMain -> addLayout (loMenu);
    loMain -> addWidget (wgGrab);
    loMain -> addWidget (sb);
    loMain -> addStretch ();

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // menu: ///////////////////////////////////////////////////////////////////////////////////////////////////////////

    QMenu *mnMain = new QMenu (this);
    pbMenu -> setMenu (mnMain);

    acLoad = new QAction (tr ("&Load..."), this);
    acLoad -> setShortcut (tr ("Ctrl+L"));
    mnMain -> addAction (acLoad);

    acSave = new QAction (tr ("&Save..."), this);
    acSave -> setShortcut (tr ("Ctrl+S"));
    acSave -> setEnabled (false);
    mnMain -> addAction (acSave);

    acSaveImage = new QAction (tr ("Save As &Image..."), this);
    acSaveImage -> setShortcut (tr ("Ctrl+I"));
    acSaveImage -> setEnabled (false);
    mnMain -> addAction (acSaveImage);

    mnMain -> addSeparator ();

    QAction *acImport = new QAction (tr ("Im&port From Clan Inquisition Trainer..."), this);
    acImport -> setShortcut (tr ("Ctrl+P"));
    mnMain -> addAction (acImport);

    acExport = new QAction (tr ("&Export To Clan Inquisition Trainer..."), this);
    acExport -> setShortcut (tr ("Ctrl+E"));
    acExport -> setEnabled (false);
    mnMain -> addAction (acExport);

    mnMain -> addSeparator ();

    QAction *acSliders = new QAction (tr ("Sho&w Sliders"), this);
    acSliders -> setShortcut (tr ("Ctrl+W"));
    acSliders -> setCheckable (true);
    acSliders -> setChecked (getBoolSetting ("sliders"));
    mnMain -> addAction (acSliders);

    QAction *acButtons = new QAction (tr ("Show +/- &Buttons"), this);
    acButtons -> setShortcut (tr ("Ctrl+B"));
    acButtons -> setCheckable (true);
    acButtons -> setChecked (getBoolSetting ("buttons"));
    mnMain -> addAction (acButtons);

    mnMain -> addSeparator ();

    acTrayClose = new QAction (tr ("Close To System Tray"), this);
    acTrayClose -> setCheckable (true);
    acTrayClose -> setChecked (getBoolSetting ("trayclose", false));
    if (! QSystemTrayIcon::isSystemTrayAvailable ()) acTrayClose -> setEnabled (false);
    mnMain -> addAction (acTrayClose);

    mnMain -> addSeparator ();

    QAction *acExit = new QAction (tr ("E&xit"), this);
    acExit -> setShortcut (tr ("Ctrl+X"));
    mnMain -> addAction (acExit);

    adjustSize ();

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // tray icon: //////////////////////////////////////////////////////////////////////////////////////////////////////

    QAction *acTrayExit    = new QAction (tr ("E&xit"), this);

    QMenu *mnTray = new QMenu (this);
    mnTray -> addAction (acTrayExit);

    tray = new QSystemTrayIcon (QIcon (":/gui/icon.png"), this);
    tray -> setContextMenu (mnTray);

    // initial tray setup:
    trayClose (getBoolSetting ("trayclose", false));

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // connections: ////////////////////////////////////////////////////////////////////////////////////////////////////

    connect (cbClass, SIGNAL (currentIndexChanged (QString)), SLOT (onClassChanged (QString)));
    connect (cbClass, SIGNAL (currentIndexChanged (QString)), SLOT (updateCbClass ()));

    connect (cbLevel, SIGNAL (currentIndexChanged (QString)), trainer, SLOT (setLevel (QString)));

    connect (pp, SIGNAL (dpTotalChanged (int)), lbDPTotal, SLOT (setNum (int)));
    connect (pp, SIGNAL (ppTotalChanged (int)), lbPPTotal, SLOT (setNum (int)));

    connect (pp, SIGNAL (dpAvailableChanged (int)), SLOT (updateDPAvailable (int)));
    connect (pp, SIGNAL (ppAvailableChanged (int)), SLOT (updatePPAvailable (int)));

    connect (pbReset, SIGNAL (clicked ()), trainer, SLOT (resetPowers ()));

    connect (acLoad,      SIGNAL (triggered ()), SLOT (load ()));
    connect (acSave,      SIGNAL (triggered ()), SLOT (save ()));
    connect (acSaveImage, SIGNAL (triggered ()), SLOT (saveImage ()));
    connect (acImport,    SIGNAL (triggered ()), SLOT (importURL ()));
    connect (acExport,    SIGNAL (triggered ()), SLOT (exportURL ()));
    connect (acExit,      SIGNAL (triggered ()), SLOT (exit ()));

    connect (acTrayExit,    SIGNAL (triggered ()), SLOT (exit    ()));

    connect (acSliders,   SIGNAL (toggled (bool)), SLOT (showSliders  (bool)));
    connect (acButtons,   SIGNAL (toggled (bool)), SLOT (showButtons  (bool)));
    connect (acTrayClose, SIGNAL (toggled (bool)), SLOT (trayClose    (bool)));

    connect (
            tray,
            SIGNAL (activated (QSystemTrayIcon::ActivationReason)),
            SLOT (restore (QSystemTrayIcon::ActivationReason))
        );

    connect (trainer, SIGNAL (spellStatus (bool, int, int)), sb, SLOT (spellStatus (bool, int, int)));

}

void TrainerGUI::updateCbClass () {

    // this will clean up the class combobox, ie. remove the first (empty) entry, and enable all initially disabled
    // or hidden widgets. this slot will be disconnected after the first call.

    disconnect (cbClass, SIGNAL (currentIndexChanged (QString)), this, SLOT (updateCbClass ()));

    cbClass -> blockSignals (true);
    cbClass -> removeItem (0);
    cbClass -> blockSignals (false);

    acSave      -> setEnabled (true);
    acSaveImage -> setEnabled (true);
    acExport    -> setEnabled (true);

    lbLevel -> show ();
    cbLevel -> show ();
    pbReset -> show ();
    wgGrab  -> show ();
    sb      -> show ();

    // though the menu show sliders/buttons menu entries may be unchecked, the default is to show both, change that if
    // required:
    if (! getBoolSetting ("sliders")) trainer -> showSliders (false);
    if (! getBoolSetting ("buttons")) trainer -> showButtons (false);

    initialized = true;

}

void TrainerGUI::onClassChanged (QString newClass, bool resetLevel) {

    // set the new spell trees for the selected class:
    trainer -> setClass (newClass);
    sb      -> setClass (newClass);

    // reset the level combobox if required and select the highest level by default:
    QPair <int, int> l = tc -> classLimits (newClass);
    int min = cbLevel -> itemText (0).toInt ();
    int max = cbLevel -> itemText (cbLevel -> count () - 1).toInt ();
    if (min != l.first || max != l.second) {
        cbLevel -> blockSignals (true);
        cbLevel -> clear ();
        for (int i = l.first; i <= l.second; i++) cbLevel -> addItem (QString ("%1").arg (i, 2, 10, QChar ('0')));
        if (resetLevel) {
            cbLevel -> blockSignals (false);
            cbLevel -> setCurrentIndex (cbLevel -> count () - 1);
        }
    }
    else if (cbLevel -> currentIndex () != cbLevel -> count () - 1 && resetLevel) {
        // no change required, but set the highest level:
        cbLevel -> setCurrentIndex (cbLevel -> count () - 1);
    }
    else if (resetLevel) {
        // highest level already selected, make sure the trainer widget is updated accordingly:
        trainer -> setLevel (cbLevel -> currentText ());
    }

}

void TrainerGUI::saveImage () {

    // this will open a save file dialog and save the current setup as an image. the menu will not be included, but
    // two labels with class name and level will be enabled, so all required information is included in the image.

    QString fn = QFileDialog::getSaveFileName (
            this,
            tr ("Save Image As..."),
            QDir::homePath (),
            tr ("Image Files") + " (*.bmp *.jpg *.jpeg *.png *.ppm *.tif *.tiff *.xbm *.xpm)"
        );

    if (fn.isEmpty ()) return;

    lbPrintClass -> setText (tc -> classNameClean (cbClass -> currentText ()));
    lbPrintLevel -> setText (tr ("Level:") + " " + cbLevel -> currentText ().remove (QRegExp ("^0+")));
    lbPrintClass -> show ();
    lbPrintLevel -> show ();

    trainer -> showSliders (false);
    QPixmap pm = QPixmap::grabWidget (wgGrab);

    lbPrintClass -> hide ();
    lbPrintLevel -> hide ();
    trainer -> showSliders (true);

    if (! pm.save (fn))
        QMessageBox::warning (
                this,
                "Regnum Online Skill Trainer",
                tr ("The image could not be saved."),
                QMessageBox::Ok
            );

}

void TrainerGUI::updateDPAvailable (int newAvail) {
    // update the available discipline points label.
    if (newAvail < 0) lbDPAvail -> setStyleSheet ("color: #f00");
    else              lbDPAvail -> setStyleSheet ("color: #000");
    lbDPAvail -> setNum (newAvail);
}

void TrainerGUI::updatePPAvailable (int newAvail) {
    // update the available power points label.
    if (newAvail < 0) lbPPAvail -> setStyleSheet ("color: #f00");
    else              lbPPAvail -> setStyleSheet ("color: #000");
    lbPPAvail -> setNum (newAvail);
}

void TrainerGUI::exit () {
    // exit the application.
    QCoreApplication::exit (0);
}

void TrainerGUI::save () {
    // save the current setup:
    if (dgSave -> exec () == QDialog::Accepted) {
        if (! ts -> saveSetup (
                    cbClass -> currentText (),
                    dgSave  -> getLastName (),
                    cbLevel -> currentText ().toInt (),
                    trainer -> encoded (),
                    sb      -> encoded (),
                    dgSave  -> getLastDescription ()
                )
            )
            QMessageBox::warning (
                    this,
                    "Regnum Online Skill Trainer",
                    tr ("The setup could not be saved."),
                    QMessageBox::Ok
                );
    }
}

void TrainerGUI::exportURL () {
    // only show the export dialog here, copy to clipboard and open browser are done from within the dialog class.
    dgImExport -> exec (ImportExportDialog::Export, trainer -> encoded (true));
}

void TrainerGUI::load () {

    // load an existing setup:
    if (dgLoad -> exec () == QDialog::Accepted) {
        TrainerSetups::tSetupInfo setup = ts -> getSetup (dgLoad -> getSetupName ());
        loadSetup (setup.code, setup.className, setup.level);
        sb -> decode (setup.spellBars);
    }

}

void TrainerGUI::importURL () {

    // show the import dialog, if accepted, try to load the setup:
    if (dgImExport -> exec (ImportExportDialog::Import) == QDialog::Accepted) {
        TrainerCodec *tc = new TrainerCodec (dgImExport -> getCode ());
        loadSetup (tc -> getCode (), tc -> getClass (), tc -> getLevel ());
    }

}

void TrainerGUI::loadSetup (QString code, QString className, int level) {

    // try to load the setup, do nothing else if this fails:
    if (! trainer -> decode (code)) {
        QMessageBox::warning (
                this,
                "Regnum Online Skill Trainer",
                tr ("There is something wrong with the specified setup."),
                QMessageBox::Ok
            );
        return;
    }

    // we have to make sure that no currentIndexChanged is triggered when setting the level combobox (setting the level
    // is done automatically by the trainer widget when when a setup is loaded), same for the class combobox:
    cbLevel -> blockSignals (true);
    cbClass -> blockSignals (true);

    if (! initialized) updateCbClass ();

    // set class combobox:
    cbClass -> setCurrentIndex (cbClass -> findText ("^\\s*" + className + "$", Qt::MatchRegExp));

    // trigger onClassChanged (with resetLevel = false!):
    onClassChanged (cbClass -> currentText (), false);

    // set the level combobox:
    cbLevel -> setCurrentIndex (cbLevel -> findText ("^0?" + QString ("%1").arg (level), Qt::MatchRegExp));

    // enable the signals again:
    cbClass -> blockSignals (false);
    cbLevel -> blockSignals (false);

}

void TrainerGUI::showButtons (bool show) {
    // enable or disable the increase/decrease buttons and save the setting.
    trainer -> showButtons (show);
    setBoolSetting ("buttons", show);
}

void TrainerGUI::showSliders (bool show) {
    // enable or disable the sliders and save the setting.
    trainer -> showSliders (show);
    setBoolSetting ("sliders", show);
}

void TrainerGUI::trayClose (bool enabled) {
    // enable or disable close to tray, if disabled, the tray icon will be removed, too. does nothing if there is no
    // systray available. the new setting will be saved to disk immediately.
    if (! QSystemTrayIcon::isSystemTrayAvailable ()) return;
    if (enabled) {
        QApplication::setQuitOnLastWindowClosed (false);
        tray -> show ();
    }
    else tray -> hide ();
    setBoolSetting ("trayclose", enabled);
}

void TrainerGUI::restore (QSystemTrayIcon::ActivationReason reason) {
    // called when the tray icon is clicked.
    if (reason == QSystemTrayIcon::Trigger) restore ();
}

void TrainerGUI::restore () {
    // restore the main window.
    QWidget *parent = parentWidget ();
    while (parent -> parentWidget ()) parent = parent -> parentWidget ();
    parent -> show ();
}

bool TrainerGUI::getBoolSetting (QString name, bool defaultValue) {
    // retrieve a stored setting:
    QSettings settings;
    return settings.value ("settings/" + name, defaultValue).toBool ();
}

void TrainerGUI::setBoolSetting (QString name, bool value) {
    // store a setting. will be written to disk immediately.
    QSettings settings;
    settings.setValue ("settings/" + name, value);
    settings.sync ();
}
