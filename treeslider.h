/*

Copyright 2011, 2014 Stefan Goebel <roskilltrainer -at- subtype.de> - Homepage: <http://subtype.de/>

This file is part of ROSkillTrainer.

ROSkillTrainer is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
version.

ROSkillTrainer is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with ROSkillTrainer. If not, see
<http://www.gnu.org/licenses/>.

*/

#ifndef TREESLIDER_H
#define TREESLIDER_H

#include "levelslider.h"
#include "pointspool.h"
#include "trainerconfig.h"

class TreeSlider: public LevelSlider {

    Q_OBJECT

    public:

        explicit TreeSlider (QWidget *parent = 0);
        void setTree (QString icon, QString tooltip);
        void reset ();

    private:

        PointsPool *pp;
        TrainerConfig *tc;
        int origLevel;

    private slots:

        void updateMaximum (int avail);
        void changePoints (int newLevel);

};

#endif // TREESLIDER_H
