/*

Copyright 2011, 2014 Stefan Goebel <roskilltrainer -at- subtype.de> - Homepage: <http://subtype.de/>

This file is part of ROSkillTrainer.

ROSkillTrainer is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
version.

ROSkillTrainer is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with ROSkillTrainer. If not, see
<http://www.gnu.org/licenses/>.

*/

#ifndef POINTSPOOL_H
#define POINTSPOOL_H

#include <QObject>

class PointsPool: public QObject {

    Q_OBJECT

    public:

        static PointsPool* pool (QObject *parent = 0);

        int getDPTotal () const;
        int getDPUsed () const;
        int getDPAvailable () const;

        int getPPTotal () const;
        int getPPUsed () const;
        int getPPAvailable () const;

    signals:

        void dpTotalChanged (int total);
        void ppTotalChanged (int total);

        void dpAvailableChanged (int avail);
        void ppAvailableChanged (int avail);

    public slots:

        void setDPTotal (int total);
        void setDPUsed (int used);

        void setPPTotal (int total);
        void setPPUsed (int used);

        void incDPUsed (int value);
        void decDPUsed (int value);

        void incPPUsed (int value);
        void decPPUsed (int value);

    private:

        explicit PointsPool (QObject *parent = 0);

        static PointsPool *instance;
        int dpUsed, dpTotal, ppUsed, ppTotal;

};

#endif // POINTSPOOL_H
