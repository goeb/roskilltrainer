/*

Copyright 2011, 2014 Stefan Goebel <roskilltrainer -at- subtype.de> - Homepage: <http://subtype.de/>

This file is part of ROSkillTrainer.

ROSkillTrainer is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
version.

ROSkillTrainer is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with ROSkillTrainer. If not, see
<http://www.gnu.org/licenses/>.

*/

#ifndef IMPORTEXPORTDIALOG_H
#define IMPORTEXPORTDIALOG_H

#include "trainercodec.h"

#include <QDialog>

#define URL_PREFIX "http://trainer.claninquisition.org/trainer.html?s="

namespace Ui {
    class ImportExportDialog;
}

class ImportExportDialog: public QDialog {

    Q_OBJECT

    public:

        enum ImportExport {
            Export = 0,
            Import = 1,
        };

        explicit ImportExportDialog (QWidget *parent = 0);
        ~ImportExportDialog ();
        QString getCode ();

    public slots:

        int exec (ImportExport type, QString code = "");

    private:

        Ui::ImportExportDialog *ui;
        QString lastCode;

        void checkCode (QString code);

    private slots:

        void copyToClipboard ();
        void openURL ();
        void checkURL ();

};

#endif // IMPORTEXPORTDIALOG_H
