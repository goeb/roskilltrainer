/*

Copyright 2011, 2014 Stefan Goebel <roskilltrainer -at- subtype.de> - Homepage: <http://subtype.de/>

This file is part of ROSkillTrainer.

ROSkillTrainer is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
version.

ROSkillTrainer is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with ROSkillTrainer. If not, see
<http://www.gnu.org/licenses/>.

*/

// provides a slider widget that is transparent to mouse events except for its actually visible parts.

#include "transparentslider.h"

#include <QBitmap>
#include <QPainter>
#include <QPaintEvent>
#include <QPixmap>

TransparentSlider::TransparentSlider (Qt::Orientation orientation, QWidget *parent): QSlider (orientation, parent) {

    // if the slider has the focus the focus indicator (border) can receive mouse events, we set the focus policy so
    // the widget can not get the focus.
    setFocusPolicy (Qt::NoFocus);

    // transparency is done by setting a mask for the widget, the mask has to be updated every time the slider is moved:
    connect (this, SIGNAL (valueChanged (int)), SLOT (updateMask ()));

}

void TransparentSlider::resizeEvent (QResizeEvent* event) {

    QSlider::resizeEvent (event);

    // update the mask on resize events:
    updateMask ();

}

void TransparentSlider::updateMask () {

    // set the mask for the slider. the slider will be rendered to a pixmap, then the mask is retrieved from that
    // pixmap:

    QPixmap mask (size ());
    mask.fill (Qt::transparent);
    QPainter::setRedirected (this, &mask);
    QPaintEvent pEvent (rect ());
    paintEvent (&pEvent);
    QPainter::restoreRedirected (this);
    setMask (mask.mask ());

}
