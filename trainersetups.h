/*

Copyright 2011, 2014 Stefan Goebel <roskilltrainer -at- subtype.de> - Homepage: <http://subtype.de/>

This file is part of ROSkillTrainer.

ROSkillTrainer is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
version.

ROSkillTrainer is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with ROSkillTrainer. If not, see
<http://www.gnu.org/licenses/>.

*/

#ifndef TRAINERSETUPS_H
#define TRAINERSETUPS_H

#include "trainerconfig.h"

#include <QObject>
#include <QPair>
#include <QSettings>
#include <QStringList>
#include <QVector>

class TrainerSetups: public QObject {

    Q_OBJECT

    public:

        struct tSetupInfo {
            QString name;
            QString code;
            QString description;
            QString className;
            QString spellBars;
            int level;
        };

        static TrainerSetups* setups (QObject *parent = 0);
        bool nameExists (QString name, bool cs = false) const;
        bool saveSetup (
                QString className, QString name, int level, QString setup, QString spellBar, QString description = ""
            );
        QVector <tSetupInfo> getSetups (QString className) const;
        void deleteSetup (QString name);
        tSetupInfo getSetup (QString name) const;

    private:

        static TrainerSetups *instance;
        TrainerConfig *tc;
        QSettings settings;
        QStringList setupNames;
        QMap <QString, QStringList*> setupsByClass;
        QMap <QString, tSetupInfo> setupInfo;

        explicit TrainerSetups (QObject *parent = 0);
        QStringList sorted (QString className) const;

    signals:

        void setupAdded (QString className, QString name, QString descr,  int level, int index);

};

#endif // TRAINERSETUPS_H
