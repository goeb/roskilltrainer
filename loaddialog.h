/*

Copyright 2011, 2014 Stefan Goebel <roskilltrainer -at- subtype.de> - Homepage: <http://subtype.de/>

This file is part of ROSkillTrainer.

ROSkillTrainer is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
version.

ROSkillTrainer is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with ROSkillTrainer. If not, see
<http://www.gnu.org/licenses/>.

*/

#ifndef LOADDIALOG_H
#define LOADDIALOG_H

#include "trainerconfig.h"
#include "trainersetups.h"

#include <QDialog>
#include <QTreeWidgetItem>

namespace Ui {
    class LoadDialog;
}

class LoadDialog: public QDialog {

    Q_OBJECT

    public:

        explicit LoadDialog (QWidget *parent = 0);
        ~LoadDialog ();
        QString getSetupName () const;

    private:

        TrainerConfig *tc;
        TrainerSetups *ts;
        bool setupsLoaded;
        Ui::LoadDialog *ui;
        QList <QTreeWidgetItem*> topItems;
        QMap <QString, QTreeWidgetItem*> namedItems;
        QString setupName;

    private slots:

        void deleteSetup ();
        void loadSetup ();
        void onItemChanged (QTreeWidgetItem *current, QTreeWidgetItem *previous);
        void onSetupAdded (QString className, QString name, QString descr, int level, int index);

};

#endif // LOADDIALOG_H
