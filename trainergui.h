/*

Copyright 2011, 2014 Stefan Goebel <roskilltrainer -at- subtype.de> - Homepage: <http://subtype.de/>

This file is part of ROSkillTrainer.

ROSkillTrainer is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
version.

ROSkillTrainer is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with ROSkillTrainer. If not, see
<http://www.gnu.org/licenses/>.

*/

#ifndef TRAINERGUI_H
#define TRAINERGUI_H

#include "importexportdialog.h"
#include "loaddialog.h"
#include "pointspool.h"
#include "savedialog.h"
#include "spellbar.h"
#include "trainerconfig.h"
#include "trainersetups.h"
#include "trainerwidget.h"

#include <QComboBox>
#include <QFrame>
#include <QLabel>
#include <QPushButton>
#include <QSystemTrayIcon>

class TrainerGUI: public QFrame {

    Q_OBJECT

    public:

        explicit TrainerGUI (QWidget *parent = 0);

    private:

        TrainerWidget *trainer;
        TrainerConfig *tc;
        TrainerSetups *ts;
        PointsPool *pp;
        QLabel *lbClass, *lbLevel;
        QLabel *lbDPAvail, *lbDPTotal;
        QLabel *lbPPAvail, *lbPPTotal;
        QLabel *lbPrintClass, *lbPrintLevel;
        QPushButton *pbReset, *pbMenu;
        QComboBox *cbClass, *cbLevel;
        QWidget *wgGrab;
        QAction *acSaveImage, *acSave, *acLoad, *acExport;
        QAction *acTrayClose;
        SpellBar *sb;
        SaveDialog *dgSave;
        LoadDialog *dgLoad;
        ImportExportDialog *dgImExport;
        bool initialized;
        QSystemTrayIcon *tray;

        void loadSetup (QString code, QString className, int level);
        bool getBoolSetting (QString name, bool defaultValue = true);
        void setBoolSetting (QString name, bool value);

    private slots:

        void onClassChanged (QString newClass, bool resetLevel = true);
        void updateCbClass ();
        void updateDPAvailable (int newAvail);
        void updatePPAvailable (int newAvail);
        void save ();
        void load ();
        void saveImage ();
        void exportURL ();
        void importURL ();
        void exit ();
        void showButtons (bool show);
        void showSliders (bool show);
        void restore ();
        void restore (QSystemTrayIcon::ActivationReason reason);
        void trayClose (bool enabled);

};

#endif // TRAINERGUI_H
