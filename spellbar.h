/*

Copyright 2011, 2014 Stefan Goebel <roskilltrainer -at- subtype.de> - Homepage: <http://subtype.de/>

This file is part of ROSkillTrainer.

ROSkillTrainer is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
version.

ROSkillTrainer is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with ROSkillTrainer. If not, see
<http://www.gnu.org/licenses/>.

*/

#ifndef SPELLBAR_H
#define SPELLBAR_H

#include "trainerconfig.h"

#include <QFrame>
#include <QLabel>
#include <QPushButton>
#include <QWheelEvent>

#define NUM_BARS   4
#define NUM_SLOTS 10

class SpellBar: public QFrame {

    Q_OBJECT

    public:

        explicit SpellBar (QWidget *parent = 0);
        void setClass (QString newClass);
        QString encoded ();
        void decode (QString code);

    public slots:

        void reset ();
        void spellStatus (bool enabled, int tNum, int sNum);

    private:

        QPushButton *pbClear;
        QList <QWidget*> spellBars;
        QList <QPair <int, int> > spellData;
        QList <QLabel*> spellLabels;
        int bar;
        TrainerConfig *tc;
        QString currentClass;

        void updateLabel (int index);
        void setBar (int newBar);

    private slots:

        void setBar1 ();
        void setBar2 ();
        void setBar3 ();
        void setBar4 ();

    protected:

        void wheelEvent (QWheelEvent *event);
        void dragEnterEvent (QDragEnterEvent *event);
        void dragMoveEvent (QDragMoveEvent *event);
        void dropEvent (QDropEvent *event);
        void mousePressEvent (QMouseEvent *event);

};

#endif // SPELLBAR_H
