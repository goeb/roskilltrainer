/*

Copyright 2011, 2014 Stefan Goebel <roskilltrainer -at- subtype.de> - Homepage: <http://subtype.de/>

This file is part of ROSkillTrainer.

ROSkillTrainer is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
version.

ROSkillTrainer is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with ROSkillTrainer. If not, see
<http://www.gnu.org/licenses/>.

*/

#include "trainerwidget.h"

#include "trainercodec.h"

#include <QVBoxLayout>

TrainerWidget::TrainerWidget (QWidget *parent): QWidget (parent) {

    QVBoxLayout *loMain = new QVBoxLayout (this);

    tc = TrainerConfig::config ();
    pp = PointsPool::pool ();

    for (int i = 0; i < tc -> maxTreeNumber (); i++) {
        TreeWidget *tree = new TreeWidget (i, this);
        trees.append (tree);
        loMain -> addWidget (tree);
        connect (tree, SIGNAL (spellStatus (bool, int, int)), SLOT (emitSignals (bool, int, int)));
    }

    currentClass = "";
    currentLevel = -1;

}

void TrainerWidget::emitSignals (bool enabled, int tNum, int sNum) {
    emit spellStatus (enabled, tNum, sNum);
}

void TrainerWidget::setClass (QString newClass) {

    // this will load the spell trees for the specified class. the widget will be hidden after this function was called,
    // you must call setLevel(), too, to show it (you may call show() explicitely, but there is no guaranteed behaviour
    // without setting the level)! this function does nothing if the current class is the same as the new one. note that
    // the used discipline and power points will be set to 0.

    if (! tc -> classExists (newClass)) return;

    newClass = tc -> classNameClean (newClass);
    if (currentClass == newClass) return;
    currentClass = newClass;

    foreach (TreeWidget *tree, trees) tree -> reset ();

    QStringList treeNames = tc -> classTrees (newClass);
    for (int i = 0; i < treeNames.size (); i++) trees.at (i) -> setTree (treeNames.at (i));

    pp -> setPPUsed (0);
    pp -> setDPUsed (0);

    currentLevel = -1;

    hide ();

}

void TrainerWidget::setLevel (int newLevel) {

    // sets the character level, or more precisely the maximum tree level based on the character level. additionally,
    // the total discipline and power points will be set (based on class and character level). this function does
    // nothing if the new level is the same as the current one except showing the widget, it also does nothing if there
    // is no class set.

    if (currentClass.isEmpty ()) return;

    // check if the level is valid for the current class:
    QPair <int, int> limits = tc -> classLimits (currentClass);
    if (newLevel < limits.first || newLevel > limits.second) return;

    if (currentLevel == newLevel) {
        show ();
        return;
    }
    currentLevel = newLevel;

    int max = tc -> treeLevel (newLevel);
    foreach (TreeWidget *tree, trees) tree -> setMaximum (max);

    pp -> setDPTotal (tc -> classDP (currentClass, newLevel));
    pp -> setPPTotal (tc -> classPP (currentClass, newLevel));

    show ();

}

void TrainerWidget::setLevel (QString newLevel) {
    // convenience method that accepts the new level as a string. see setLevel(int).
    setLevel (newLevel.toInt ());
}

void TrainerWidget::resetPowers () {
    // same as in game /reset_powers command, ie. all trees and spells will be set to the minimum level.
    foreach (TreeWidget *tree, trees) tree -> resetPowers ();
}

void TrainerWidget::showSliders (bool show) {
    // if <show> is true, sliders will be displayed, else they will be hidden.
    foreach (TreeWidget *tree, trees) tree -> setShowSlider (show);
}

void TrainerWidget::showButtons (bool show) {
    // if <show> is true, buttons will be displayed on mouseover, else they will be hidden.
    foreach (TreeWidget *tree, trees) tree -> setButtonsEnabled (show);
}

QVector <int> TrainerWidget::getTreeLevels () const {
    QVector <int> result;
    foreach (TreeWidget *tree, trees)
        // unused trees are hidden:
        if (! tree -> isHidden ()) result.append (tree -> getLevel ());
        else break;
    return result;
}

QVector <QVector <int> > TrainerWidget::getSpellLevels () const {
    QVector <QVector <int> > result;
    foreach (TreeWidget *tree, trees)
        // unused trees are hidden:
        if (! tree -> isHidden ()) result.append (tree -> getSpellLevels ());
        else break;
    return result;
}

QString TrainerWidget::encoded (bool compat) const {
    // we should always have a valid setup if a class and level are selected, so no further checks apart from these are
    // required. if no class or level is set we return an empty string.
    if (currentClass.isEmpty () || currentLevel == -1) return "";
    TrainerCodec *codec = new TrainerCodec (currentClass, currentLevel, getTreeLevels (), getSpellLevels ());
    return codec -> getCode (compat);
}

bool TrainerWidget::decode (QString code) {
    // load the setup specified by <code>. if the code is not valid for some reason this method will return false, else
    // true. level, class and spell levels will be set accordingly.
    TrainerCodec *codec = new TrainerCodec (code);
    if (! codec -> isValid ()) return false;
    resetPowers ();
    setClass (codec -> getClass ());
    setLevel (codec -> getLevel ());
    QVector <int> treeLevels = codec -> getTreeLevels ();
    QVector <QVector <int> > spellLevels = codec -> getSpellLevels ();
    for (int i = 0; i < treeLevels.size (); i++) {
        trees.at (i) -> setLevel (treeLevels.at (i));
        trees.at (i) -> setSpellLevels (spellLevels.at (i));
    }
    return true;
}
