/*

Copyright 2011, 2014 Stefan Goebel <roskilltrainer -at- subtype.de> - Homepage: <http://subtype.de/>

This file is part of ROSkillTrainer.

ROSkillTrainer is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
version.

ROSkillTrainer is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with ROSkillTrainer. If not, see
<http://www.gnu.org/licenses/>.

*/

// handles storing and loading setups from disk. important: setup names have to be unique. a setup with a specified name
// can not be saved if another setup with that name exists. in this case the names are compared case insensitive.
// however, when loading a setup, you have to spell the name exactly as stored on disk, lookup is case sensitive!

#include "trainersetups.h"

TrainerSetups *TrainerSetups::instance = 0;

TrainerSetups* TrainerSetups::setups (QObject *parent) {
    if (! instance) instance = new TrainerSetups (parent);
    return instance;
}

TrainerSetups::TrainerSetups (QObject *parent): QObject (parent) {

    tc = TrainerConfig::config (this);

    // load all setups from the configuration. there are three variables to store them:
    //  setupNames      string list containing all setup names for easy case insensitive lookup if a name exists
    //  setupsByClass   map <string, string list>, the list contains all setups for the class in config file order
    //  setupInfo       map <string, setup info>, the actual setups

    foreach (QString className, tc -> classStringList (false)) {

        int size = settings.beginReadArray ("setups/" + className);

        if (! setupsByClass.contains (className)) setupsByClass.insert (className, new QStringList ());

        for (int i = 0; i < size; i++) {

            settings.setArrayIndex (i);

            QString name = settings.value ("name").toString ();

            setupNames.append (name);
            setupsByClass.value (className) -> append (name);

            tSetupInfo info;
            info.name        = name;
            info.className   = className;
            info.code        = settings.value ("code").toString ();
            info.description = settings.value ("desc").toString ();
            info.spellBars   = settings.value ("bars").toString ();
            info.level       = settings.value ("level").toInt ();
            setupInfo.insert (name, info);

        }

        settings.endArray ();

    }

}

bool TrainerSetups::nameExists (QString name, bool cs) const {
    // returns true if a setup with the given name already exists in any class. search is performed case insensitive.
    return setupNames.contains (name, cs ? Qt::CaseSensitive : Qt::CaseInsensitive);
}

bool TrainerSetups::saveSetup (
        QString className, QString name, int level, QString setup, QString spellBar, QString description
    ) {

    // saves a new setup. this function does nothing if a setup with the specified name already exists. the setup will
    // be written to disk immediately. a signal (setupAdded()) will be emitted, containing some data about the new
    // setup. note that setups with the same name as a class can not be saved.

    if (nameExists (name) || tc -> classExists (name, Qt::CaseInsensitive)) return false;

    className = tc -> classNameClean (className);

    // add the new entry to the end of the config:
    settings.beginWriteArray ("setups/" + className);
    settings.setArrayIndex (setupsByClass.value (className) -> size ());
    settings.setValue ("name",  name);
    settings.setValue ("level", level);
    settings.setValue ("code",  setup);
    settings.setValue ("desc",  description);
    settings.setValue ("bars",  spellBar);
    settings.endArray ();
    settings.sync ();

    // add the new entry to the internal lists:
    setupNames.append (name);
    setupsByClass.value (className) -> append (name);

    tSetupInfo info;
    info.name        = name;
    info.className   = className;
    info.code        = setup;
    info.description = description;
    info.level       = level;
    info.spellBars   = spellBar;
    setupInfo.insert (name,  info);

    // emit the setupAdded signal:
    emit setupAdded (className, name, description, level, sorted (className).indexOf (name));

    return true;

}

void TrainerSetups::deleteSetup (QString name) {

    // deletes the specified setup. will be written to disk immediately.

    if (! nameExists (name, true)) return;

    QString className = setupInfo.value (name).className;

    // remove the setup from the internal lists:
    setupNames.removeOne (name);
    setupsByClass.value (className) -> removeOne (name);
    setupInfo.remove (name);

    // qsettings does not handle removing items from an array very well, the safest way is to remove all entries for the
    // class and rewrite the remaining entries afterwards.

    settings.remove ("setups/" + className);
    settings.beginWriteArray ("setups/" + className);
    for (int i = 0; i < setupsByClass.value (className) -> size (); i++) {
        QString tmp = setupsByClass.value (className) -> at (i);
        settings.setArrayIndex (i);
        settings.setValue ("name",  tmp);
        settings.setValue ("code",  setupInfo.value (tmp).code);
        settings.setValue ("desc",  setupInfo.value (tmp).description);
        settings.setValue ("bars",  setupInfo.value (tmp).spellBars);
        settings.setValue ("level", setupInfo.value (tmp).level);
    }
    settings.endArray();
    settings.sync ();

}

QStringList TrainerSetups::sorted (QString className) const {
    // returns the setup names for the specified class sorted by name (case insensitive).
    QMap <QString, QString> map;
    for (int i = 0; i < setupsByClass.value (className) -> size (); i++) {
        QString str = setupsByClass.value (className) -> at (i);
        map.insert (str.toLower (), str);
    }
    return map.values ();
}

QVector <TrainerSetups::tSetupInfo> TrainerSetups::getSetups (QString className) const {
    // returns the (sorted) list of setups for the specified class.
    QVector <tSetupInfo> result;
    className.remove (QRegExp ("^\\s*"));
    foreach (QString str, sorted (className)) result.append (setupInfo.value (str));
    return result;
}

TrainerSetups::tSetupInfo TrainerSetups::getSetup (QString name) const {
    return nameExists (name, true) ? setupInfo.value (name) : tSetupInfo ();
}
