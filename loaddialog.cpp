/*

Copyright 2011, 2014 Stefan Goebel <roskilltrainer -at- subtype.de> - Homepage: <http://subtype.de/>

This file is part of ROSkillTrainer.

ROSkillTrainer is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
version.

ROSkillTrainer is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with ROSkillTrainer. If not, see
<http://www.gnu.org/licenses/>.

*/

#include "loaddialog.h"
#include "ui_loaddialog.h"

LoadDialog::LoadDialog (QWidget *parent): QDialog (parent), ui (new Ui::LoadDialog) {

    setWindowFlags (Qt::Dialog | Qt::CustomizeWindowHint | Qt::WindowTitleHint | Qt::WindowCloseButtonHint);

    tc = TrainerConfig::config (this);
    ts = TrainerSetups::setups (this);

    setupName = "";

    ui -> setupUi (this);

    ui -> twSetups -> setColumnCount (2);
    ui -> twSetups -> header () -> setResizeMode (0, QHeaderView::Stretch);
    ui -> twSetups -> header () -> setResizeMode (1, QHeaderView::ResizeToContents);
    ui -> twSetups -> header () -> setStretchLastSection (false);

    // add the class and subclass items to the treewidget:
    QVector <QPair <QString, bool> > classes = tc -> classList ();
    for (int i = 0; i < classes.size (); i++) {
        QString className = classes.at (i).first;
        if (! classes.at (i).second) {
            topItems.append (new QTreeWidgetItem ((QTreeWidget*) 0, QStringList (className)));
            namedItems.insert (className, topItems.last ());
        }
        else
            namedItems.insert (className, new QTreeWidgetItem (topItems.last (), QStringList (className)));
        namedItems.value (className) -> setFont (0, QFont ("", -1, QFont::Bold, false));
    }
    ui -> twSetups -> insertTopLevelItems (0, topItems);

    // iterate over the class list again, this time adding the stored setups for the classes.
    for (int i = 0; i < classes.size (); i++) {
        QString className = classes.at (i).first;
        QVector <TrainerSetups::tSetupInfo> setups = ts -> getSetups (className);
        foreach (TrainerSetups::tSetupInfo setup, setups) {
            QTreeWidgetItem *newItem = new QTreeWidgetItem (
                    namedItems.value (className),
                    QStringList () << setup.name << QString ("%1").arg (setup.level)
                );
            newItem -> setToolTip (0, setup.description);
            newItem -> setToolTip (1, setup.description);
        }
    }

    connect (ui -> pbCancel, SIGNAL (clicked ()), SLOT (reject ()));
    connect (ui -> pbDelete, SIGNAL (clicked ()), SLOT (deleteSetup ()));
    connect (ui -> pbLoad,   SIGNAL (clicked ()), SLOT (loadSetup()));

    connect (
            ui -> twSetups,
            SIGNAL (currentItemChanged (QTreeWidgetItem*, QTreeWidgetItem*)),
            SLOT   (onItemChanged (QTreeWidgetItem*, QTreeWidgetItem*))
        );
    connect (ui -> twSetups, SIGNAL (doubleClicked (QModelIndex)), SLOT (loadSetup ()));

    connect (
            ts,
            SIGNAL (setupAdded (QString, QString, QString, int, int)),
            SLOT   (onSetupAdded (QString, QString, QString, int, int))
        );

}

LoadDialog::~LoadDialog () {
    delete ui;
}

void LoadDialog::onItemChanged (QTreeWidgetItem *current, QTreeWidgetItem*) {

    // enable/disable buttons as required. note: we use the second column as an indicator if the item is a top level one
    // (ie. a class name) or a setup item. do not set any value for the top level items' second column!

    bool enable = ! current -> text (1).isEmpty ();
    ui -> pbDelete -> setEnabled (enable);
    ui -> pbLoad   -> setEnabled (enable);

}

void LoadDialog::deleteSetup () {

    // delete the currently selected setup:

    QTreeWidgetItem *item = ui -> twSetups -> currentItem ();
    if (item -> text (1).isEmpty ()) return;
    ts -> deleteSetup (item -> text (0));
    delete item;

}

void LoadDialog::loadSetup () {

    // not only loading a setup, but generally handling of double click events. on top level items this method will
    // expand or collapse the item, else it will accept() the dialog.

    QTreeWidgetItem *item = ui -> twSetups -> currentItem ();
    if (item -> text (1).isEmpty ()) {
        // this doesn't really make sense to me, maybe a qt bug?
        item -> setExpanded (item -> isExpanded ());
    }
    else {
        setupName = item -> text (0);
        accept ();
    }

}

QString LoadDialog::getSetupName () const {
    // returns the selected setup's name.
    return setupName;
}

void LoadDialog::onSetupAdded (QString className, QString name, QString descr, int level, int index) {

    // this will insert a new setup named <name>, the description <descr> and the level <level> for the character class
    // <className> at position <index> into the tree widget. should be connected to the TrainerSetups class' setupAdded
    // signal. <index> will be correct as long as all deletions and additions are synced.

    // actual index is <index> + 2 for main classes, since there are sublass items at index 0 and 1:
    int offset = (topItems.contains (namedItems.value (className))) ? tc -> numSubClasses (className) : 0;

    // insert the item:
    QTreeWidgetItem *newItem = new QTreeWidgetItem (
            (QTreeWidgetItem*) 0, QStringList () << name << QString ("%1").arg (level)
        );
    newItem -> setToolTip (0, descr);
    newItem -> setToolTip (1, descr);
    namedItems.value (className) -> insertChild (index + offset, newItem);

}
