/*

Copyright 2011, 2014 Stefan Goebel <roskilltrainer -at- subtype.de> - Homepage: <http://subtype.de/>

This file is part of ROSkillTrainer.

ROSkillTrainer is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
version.

ROSkillTrainer is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with ROSkillTrainer. If not, see
<http://www.gnu.org/licenses/>.

*/

#include "treewidget.h"
#include "resources.h"

#include <QFile>

TreeWidget::TreeWidget (int tNum, QWidget *parent): QGroupBox (parent) {

    hide ();

    setFixedSize (515, 65);
    setFlat (true);
    setTitle ("");

    tc = TrainerConfig::config (this);
    pp = PointsPool::pool (this);

    treeNum = tNum;

    tree = new TreeSlider (this);
    tree -> setGeometry (5, 25, 39, 39);
    tree -> setEnabled (true);

    for (int i = 0; i < tc -> spellNumber (); i++) {
        spells.append (new SpellSlider (treeNum, i, this));
        spells.last () -> setGeometry (60 + i * 46, 25, 39, 39);
        connect (spells.last (), SIGNAL (spellStatus (bool, int, int)), SLOT (emitSignals (bool, int, int)));
    }

    slider = new TransparentSlider (Qt::Horizontal, this);
    slider -> setStyleSheet (
            "QSlider::groove:horizontal {"
                "border: none;"
                "height: 5px;"
                "margin: 2px 0;"
            "}"
            "QSlider::handle:horizontal {"
                "background-image: url(\":/gui/slider.png\");"
                "border: 1px solid #5c5c5c;"
                "width: 7px;"
                "border-radius: 4px;"
                "margin: -6px 0;"
            "}"
        );
    slider -> setSingleStep (1);
    slider -> setPageStep (1);
    slider -> setMinimum (1);
    slider -> setMaximum (tc -> maxTreeLevel ());
    slider -> setGeometry (72, 12, 423, 25);

    connect (slider, SIGNAL (actionTriggered (int)), this,   SLOT (checkSliderMax      ()));
    connect (tree,   SIGNAL (valueChanged    (int)), slider, SLOT (setValue            (int)));
    connect (tree,   SIGNAL (valueChanged    (int)), this,   SLOT (enableSpells        ()));
    connect (tree,   SIGNAL (valueChanged    (int)), this,   SLOT (updateSliderToolTip ()));

}

void TreeWidget::emitSignals (bool enabled, int tNum, int sNum) {
    emit spellStatus (enabled, tNum, sNum);
}

void TreeWidget::setTree (QString treeDir) {

    // if a file can not be found/opened this method will set default values for the texts, and a fallback icon for all
    // icons that are not available. the widget will be shown when this function is called.

    // set icon, tooltip & title:
    QString description = Resources::getText (":/spells/" + treeDir + "/description." + tr ("en"));
    tree -> setTree (Resources::getIcon (":/spells/" + treeDir + "/icon.png"), description);
    description.remove (QRegExp ("\\n.*"));
    setTitle (description);

    // spells:
    for (int i = 0; i < spells.size (); i++) {
        description = Resources::getText (QString (":/spells/" + treeDir + "/%1." + tr ("en")).arg (i));
        spells.at (i) -> setSpell (
                Resources::getIcon (QString (":/spells/" + treeDir + "/%1.jpg").arg (i)),
                description,
                QFile::exists (QString (":/spells/" + treeDir + "/%1.passive").arg (i))
            );
    }
    enableSpells ();
    updateSliderToolTip ();

    show ();

}

void TreeWidget::setLevel (int level) {
    // minimum and maximum are set, no checks necessary.
    tree -> setValue (level);
}

void TreeWidget::enableSpells () {
    // enable all spells according to the current tree level and set their maximum spell level, disable the others.
    int level = tree -> value ();
    int max   = tc -> spellLevel (level);
    int upTo  = (level % 2 == 0) ? level / 2 : (level + 1) / 2;
    for (int i = 0; i < spells.size (); i++) {
        spells.at (i) -> setEnabled (i < upTo);
        if (i < upTo) spells.at (i) -> setMaximum (max);
    }
}

void TreeWidget::checkSliderMax () {

    int newLevel = slider -> sliderPosition ();
    int oldLevel = tree -> value ();

    if (newLevel == oldLevel) return;

    // try to set the new level:
    tree -> setValue (newLevel);

    // bugfix: it is possible to move the slider very fast and cause a tree level to be set exceeding the available
    // discipline points, probably the level cap is not set fast enough, we check the number of discipline points after
    // the setValue() call and reset the stuff to the old value if it is below zero:

    if (pp -> getDPAvailable () < 0) {
        tree -> setValue (oldLevel);
        slider -> setSliderPosition (oldLevel);
    }

    // if the new level could not be set reset the slider to the current level:
    if (tree -> value () != newLevel) slider -> setSliderPosition (tree -> value ());

    updateSliderToolTip ();

}

void TreeWidget::setMaximum (int level) {
    if (level > tc -> maxTreeLevel ()) return;
    tree -> setMaximum (level);
    updateSliderToolTip ();
}

void TreeWidget::reset () {
    hide ();
    setTitle ("");
    slider -> setSliderPosition (1);
    tree -> reset ();
    foreach (SpellSlider *spell, spells) spell -> reset ();
}

void TreeWidget::updateSliderToolTip () {
    int level = tree -> value ();
    if (level == tree -> maximum ()) slider -> setToolTip ("Already at maximum level.");
    else {
        int cost = tc -> dpCost (level);
        slider -> setToolTip (QString ("%1 discipline points required to next level.").arg (cost));
    }
}

void TreeWidget::setShowSlider (bool enabled) {
    if (enabled) slider -> show ();
    else         slider -> hide ();
}

void TreeWidget::setButtonsEnabled (bool enabled) {
    tree -> setButtonsEnabled (enabled);
    foreach (SpellSlider *spell, spells) spell -> setButtonsEnabled (enabled);
}

int TreeWidget::getLevel () const {
    return tree -> value ();
}

QVector <int> TreeWidget::getSpellLevels () const {
    QVector <int> result;
    foreach (SpellSlider *spell, spells) result.append (spell -> value ());
    return result;
}

void TreeWidget::setSpellLevels (QVector <int> levels) {
    for (int i = 0; i < levels.size (); i++) spells.at (i) -> setValue (levels.at (i));
}

void TreeWidget::resetPowers () {
    tree -> setValue (1);
    foreach (SpellSlider *spell, spells) spell -> setValue (0);
    updateSliderToolTip ();
}
