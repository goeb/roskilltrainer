/*

Copyright 2011, 2014 Stefan Goebel <roskilltrainer -at- subtype.de> - Homepage: <http://subtype.de/>

This file is part of ROSkillTrainer.

ROSkillTrainer is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
version.

ROSkillTrainer is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with ROSkillTrainer. If not, see
<http://www.gnu.org/licenses/>.

*/

#include "spellslider.h"

#include <QDrag>
#include <QMimeData>

SpellSlider::SpellSlider (int tNum, int sNum, QWidget *parent): LevelSlider (parent) {

    hide ();

    tc = TrainerConfig::config ();
    pp = PointsPool::pool ();
    connect (pp, SIGNAL (ppAvailableChanged (int)), SLOT (updateMaximum (int)));

    setFixedSize (39, 39);
    setTextStyle (25, 24, 14, 14, "background-color: #ccc; color: #fff; font-weight: bold;");

    setMinimum (0);
    setMaximum (tc -> spellLevel (1));
    setValue   (0);

    connect (this, SIGNAL (valueChanged (int)), SLOT (changePoints (int)));
    connect (this, SIGNAL (valueChanged (int)), SLOT (emitSignals  (int)));

    origVal  = 0;
    treeNum  = tNum;
    spellNum = sNum;
    passive  = false;

    updateMaximum (pp -> getPPAvailable ());
    setEnabled (false);

}

void SpellSlider::setSpell (QString icon, QString tooltip, bool isPassive) {
    setIcon (icon);
    setToolTip (tooltip);
    passive = isPassive;
    show ();
}

void SpellSlider::updateMaximum (int avail) {
    if (avail <= 0) setTempCap (-1);
    else releaseTempCap ();
}

void SpellSlider::emitSignals (int newValue) {
    emit spellStatus (newValue != 0, treeNum, spellNum);
}

void SpellSlider::changePoints (int newValue) {
    if (origVal != newValue) {
        pp -> incPPUsed (newValue - origVal);
        origVal = newValue;
    }
    if (newValue == 0) setTextStyleSheet ("background-color: #ccc; color: #fff; font-weight: bold;");
    else               setTextStyleSheet ("background-color: #000; color: #fff; font-weight: bold;");
}

void SpellSlider::reset () {
    hide ();
    LevelSlider::reset ();
    setMaximum (tc -> spellLevel (1));
    setToolTip ("");
    origVal = 0;
    passive = false;
    updateMaximum (pp -> getPPAvailable ());
    setEnabled (false);
}

void SpellSlider::mousePressEvent (QMouseEvent *event) {

    if (passive || value () == 0) return;

    QLabel *child = static_cast <QLabel*> (childAt (event -> pos ()));
    if (! child || ! child -> pixmap () || child -> pixmap () -> isNull ()) return;

    QPixmap pixmap = *child -> pixmap ();
    pixmap = pixmap.scaled (32, 32, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);

    QByteArray itemData;
    QDataStream dataStream (&itemData, QIODevice::WriteOnly);
    dataStream << (qint8) treeNum << (qint8) spellNum;

    QMimeData *mimeData = new QMimeData;
    mimeData -> setData ("application/x-rospelldata", itemData);

    QDrag *drag = new QDrag (this);
    drag -> setMimeData (mimeData);
    drag -> setPixmap (pixmap);
    drag -> setHotSpot (event -> pos () - child -> pos ());

    drag -> exec (Qt::LinkAction);

}
