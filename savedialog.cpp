/*

Copyright 2011, 2014 Stefan Goebel <roskilltrainer -at- subtype.de> - Homepage: <http://subtype.de/>

This file is part of ROSkillTrainer.

ROSkillTrainer is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
version.

ROSkillTrainer is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with ROSkillTrainer. If not, see
<http://www.gnu.org/licenses/>.

*/

#include "savedialog.h"
#include "ui_savedialog.h"

SaveDialog::SaveDialog (QWidget *parent): QDialog (parent), ui (new Ui::SaveDialog) {

    ts = TrainerSetups::setups (this);
    tc = TrainerConfig::config (this);

    name = "";
    description = "";

    setWindowFlags (Qt::Dialog | Qt::CustomizeWindowHint | Qt::WindowTitleHint | Qt::WindowCloseButtonHint);

    ui -> setupUi (this);
    ui -> lbWarning -> hide ();

    connect (ui -> leName,   SIGNAL (textEdited (QString)), SLOT (onNameEdited (QString)));
    connect (ui -> pbCancel, SIGNAL (clicked ()),           SLOT (reject ()));
    connect (ui -> pbSave,   SIGNAL (clicked ()),           SLOT (save ()));

}

SaveDialog::~SaveDialog () {
    delete ui;
}

void SaveDialog::onNameEdited (QString newName) {

    newName = tc -> classNameClean (newName);

    // if name is empty disable the save button and hide the warning label.
    if (newName.isEmpty ()) {
        ui -> pbSave -> setEnabled (false);
        ui -> lbWarning -> hide ();
        return;
    }

    // show the warning if required:
    if (tc -> classExists (newName, Qt::CaseInsensitive)) {
        ui -> lbWarning -> setText (tr ("This setup name is not valid."));
        ui -> lbWarning -> show ();
        ui -> pbSave -> setEnabled (false);
    }
    else if (ts -> nameExists (newName)) {
        ui -> lbWarning -> setText (tr ("A setup with this name already exists."));
        ui -> lbWarning -> show ();
        ui -> pbSave -> setEnabled (false);
    }
    else {
        ui -> lbWarning -> hide ();
        ui -> pbSave -> setEnabled (true);
    }

}

QString SaveDialog::getLastName ()        const { return name;        }
QString SaveDialog::getLastDescription () const { return description; }

void SaveDialog::reject () {

    // clear the fields:
    ui -> leName -> clear ();
    ui -> teDescription -> clear ();

    // and close the dialog:
    QDialog::reject ();

}

void SaveDialog::save () {

    // save the values:
    name = ui -> leName -> text ();
    description = ui -> teDescription -> toPlainText ();

    // remove leading and trailing spaces:
    name.remove (QRegExp ("^\\s*"));
    name.remove (QRegExp ("\\s*$"));
    description.remove (QRegExp ("^\\s*"));
    description.remove (QRegExp ("\\s*$"));

    // and clear the fields:
    ui -> leName -> clear ();
    ui -> teDescription -> clear ();

    // close the dialog:
    accept ();

}
