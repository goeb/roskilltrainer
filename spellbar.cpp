/*

Copyright 2011, 2014 Stefan Goebel <roskilltrainer -at- subtype.de> - Homepage: <http://subtype.de/>

This file is part of ROSkillTrainer.

ROSkillTrainer is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
version.

ROSkillTrainer is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with ROSkillTrainer. If not, see
<http://www.gnu.org/licenses/>.

*/

#include "spellbar.h"

#include "resources.h"

#include <QShortcut>
#include <QGraphicsColorizeEffect>
#include <QHBoxLayout>

SpellBar::SpellBar (QWidget *parent): QFrame (parent) {

    QHBoxLayout *loMain = new QHBoxLayout (this);

    // clear bars button:
    pbClear = new QPushButton (tr ("Cle&ar"), this);
    loMain -> addStretch ();
    loMain -> addWidget (pbClear);

    // create the spell bars 1 - NUM_BARS:
    for (int i = 0; i < NUM_BARS; i++) {

        // a widget as container for the labels, for easy hiding/showing individual bars:
        QWidget *container = new QWidget (this);
        QHBoxLayout *loContainer = new QHBoxLayout (container);

        // create the labels (and effects) and set the initial spell data:
        for (int j = 0; j < NUM_SLOTS; j++) {

            QLabel *lbSpell = new QLabel (container);
            QGraphicsColorizeEffect *efSpell = new QGraphicsColorizeEffect (lbSpell);

            efSpell -> setColor (QColor (128, 128, 128));
            efSpell -> setEnabled (false);

            lbSpell -> setNum ((j + 1) % 10);
            lbSpell -> setFrameShape (Box);
            lbSpell -> setFrameShadow (Plain);
            lbSpell -> setAlignment (Qt::AlignCenter | Qt::AlignHCenter);
            lbSpell -> setFixedSize (32, 32);
            lbSpell -> setScaledContents (true);
            lbSpell -> setGraphicsEffect (efSpell);

            loContainer -> addWidget (lbSpell);
            if (j == ((NUM_SLOTS + NUM_SLOTS % 2) / 2) - 1) loContainer -> addSpacing (7);

            spellLabels.append (lbSpell);
            spellData.append (QPair <int, int> (-1, -1));

        }

        // current spell bar indicator:
        QLabel *lbBarNum = new QLabel (container);
        lbBarNum -> setFixedSize (7, 32);
        lbBarNum -> setPixmap (QPixmap (QString (":/gui/sb_%1.png").arg (i)));

        loContainer -> addSpacing (7);
        loContainer -> addWidget (lbBarNum);

        spellBars.append (container);

        loMain -> addWidget (container);
        if (i != 0) container -> hide ();

    }

    bar = 0;
    tc = TrainerConfig::config (this);

    setAcceptDrops (true);

    connect (new QShortcut (QKeySequence ("F1"), parent), SIGNAL (activated ()), SLOT (setBar1 ()));
    connect (new QShortcut (QKeySequence ("F2"), parent), SIGNAL (activated ()), SLOT (setBar2 ()));
    connect (new QShortcut (QKeySequence ("F3"), parent), SIGNAL (activated ()), SLOT (setBar3 ()));
    connect (new QShortcut (QKeySequence ("F4"), parent), SIGNAL (activated ()), SLOT (setBar4 ()));

    connect (pbClear, SIGNAL (clicked ()), SLOT (reset ()));

}

void SpellBar::setBar1 () { setBar (0); }
void SpellBar::setBar2 () { setBar (1); }
void SpellBar::setBar3 () { setBar (2); }
void SpellBar::setBar4 () { setBar (3); }

void SpellBar::reset () {
    // removes all spellbar entries and sets the current bar to 0:
    for (int i = 0; i < NUM_BARS; i++) {
        setBar (i);
        for (int j = 0; j < NUM_SLOTS; j++) {
            spellData.replace (i * NUM_SLOTS + j, QPair <int, int> (-1, -1));
            updateLabel (j);
        }
    }
    setBar (0);
}

void SpellBar::updateLabel (int index) {

    // update the label at index <index> of the current bar:

    int realIndex = bar * NUM_SLOTS + index;
    QLabel *l = spellLabels.at (realIndex);
    int t = spellData.at (realIndex).first;
    int s = spellData.at (realIndex).second;

    // disable the graphics effect:
    spellLabels.at (realIndex) -> graphicsEffect () -> setEnabled (false);

    // no spell set for the label, clear it:
    if (t == -1 || s == -1) {
        l -> setPixmap (0);
        l -> setNum ((index + 1) % 10);
        l -> setFrameShape (Box);
    }

    // spell set, load icon & tooltip:
    else {
        QString treeName = tc -> classTrees (currentClass).at (t);
        QString ico = QString (":/spells/%1/%2.jpg").arg (treeName).arg (s);
        QString txt = QString (":/spells/%1/%2." + tr ("en")).arg (treeName).arg (s);
        l -> setPixmap (QPixmap (Resources::getIcon (ico)));
        l -> setToolTip (Resources::getText (txt));
        l -> setFrameShape (NoFrame);
    }

}

void SpellBar::spellStatus (bool enabled, int tNum, int sNum) {
    // enable or disable spell bar entries if the corresponding spell has been enabled or disabled.
    for (int i = 0; i < spellData.size (); i++) {
        if (spellData.at (i) == QPair <int, int> (tNum, sNum))
            spellLabels.at (i) -> graphicsEffect () -> setEnabled (! enabled);
    }
}

void SpellBar::wheelEvent (QWheelEvent *event) {
    // mouse wheel changes the current bar:
    setBar (event -> delta () < 0 ? bar + 1 : bar - 1);
}

void SpellBar::setBar (int newBar) {
    // sets the current bar:
    if (newBar == bar) return;
    if (newBar >= NUM_BARS) newBar = 0;
    if (newBar < 0) newBar = NUM_BARS - 1;
    spellBars.at (bar) -> hide ();
    bar = newBar;
    spellBars.at (bar) -> show ();
}

void SpellBar::setClass (QString newClass) {
    // sets the current class and resets the spell bars:
    if (! tc -> classExists (newClass)) return;
    newClass = tc -> classNameClean (newClass);
    if (currentClass == newClass) return;
    currentClass = newClass;
    reset ();
}

void SpellBar::dragEnterEvent (QDragEnterEvent *event) {

    // accept drag enter events if the correct mime type is set:

    if (event -> mimeData () -> hasFormat ("application/x-rospelldata")) {
        event -> acceptProposedAction ();
    }

    else if (event -> mimeData () -> hasFormat ("application/x-rospellbardata")) {
        event -> acceptProposedAction ();
    }

    else event -> ignore ();

}

void SpellBar::dragMoveEvent (QDragMoveEvent *event) {

    // accept drag move events if the correct mime data is set and there is a label at the cursor position:

    if (event -> mimeData () -> hasFormat ("application/x-rospelldata")) {

        QLabel *child = static_cast <QLabel*> (childAt (event -> pos ()));
        if (child && spellLabels.contains (child)) {
            event -> acceptProposedAction ();
            return;
        }

    }

    else if (event -> mimeData () -> hasFormat ("application/x-rospellbardata")) {

        QLabel *child = static_cast <QLabel*> (childAt (event -> pos ()));

        if (child && spellLabels.contains (child)) {

            QByteArray itemData = event -> mimeData () -> data ("application/x-rospellbardata");
            QDataStream dataStream (&itemData, QIODevice::ReadOnly);
            qint8 spellIndex;
            dataStream >> spellIndex;

            // do not allow dropping to the source widget:
            if (spellLabels.indexOf (child) != spellIndex) {
                event -> acceptProposedAction ();
                return;
            }

        }

    }

    event -> ignore ();

}

void SpellBar::dropEvent (QDropEvent *event) {

    // handle the drop event (if appropriate), set the spell data and update the label:

    if (event -> mimeData () -> hasFormat ("application/x-rospelldata")) {

        QLabel *child = static_cast <QLabel*> (childAt (event -> pos ()));

        if (child && spellLabels.contains (child)) {

            QByteArray itemData = event -> mimeData () -> data ("application/x-rospelldata");
            QDataStream dataStream (&itemData, QIODevice::ReadOnly);
            qint8 treeNum, spellNum;
            dataStream >> treeNum >> spellNum;

            int spellIndex = spellLabels.indexOf (child);
            spellData.replace (spellIndex, QPair <int, int> (treeNum, spellNum));

            event -> acceptProposedAction ();

            updateLabel (spellIndex % NUM_SLOTS);

            return;

        }

    }

    else if (event -> mimeData () -> hasFormat ("application/x-rospellbardata")) {

        QLabel *child = static_cast <QLabel*> (childAt (event -> pos ()));

        if (child && spellLabels.contains (child)) {

            QByteArray itemData = event -> mimeData () -> data ("application/x-rospellbardata");
            QDataStream dataStream (&itemData, QIODevice::ReadOnly);
            qint8 spellIndexOld;
            dataStream >> spellIndexOld;
            qint8 spellIndexNew = spellLabels.indexOf (child);

            if (spellIndexNew != spellIndexOld) {

                QPair <int, int> dataOld = QPair <int, int> (spellData.at (spellIndexOld));
                QPair <int, int> dataNew = QPair <int, int> (spellData.at (spellIndexNew));

                bool disabledOld = spellLabels.at (spellIndexOld) -> graphicsEffect () -> isEnabled ();
                bool disabledNew = spellLabels.at (spellIndexNew) -> graphicsEffect () -> isEnabled ();

                spellData.replace (spellIndexOld, dataNew);
                spellData.replace (spellIndexNew, dataOld);

                updateLabel (spellIndexNew % NUM_SLOTS);
                updateLabel (spellIndexOld % NUM_SLOTS);

                spellLabels.at (spellIndexNew) -> graphicsEffect () -> setEnabled (disabledOld);
                spellLabels.at (spellIndexOld) -> graphicsEffect () -> setEnabled (disabledNew);

            }

        }

    }

    event -> ignore ();

}

void SpellBar::mousePressEvent (QMouseEvent *event) {

    // remove a spell from the bar on right click:

    if (event -> button () == Qt::RightButton) {

        QLabel *child = static_cast <QLabel*> (childAt (event -> pos ()));

        if (child && spellLabels.contains (child)) {
            int spellIndex = spellLabels.indexOf (child);
            spellData.replace (spellIndex, QPair <int, int> (-1, -1));
            updateLabel (spellIndex % NUM_SLOTS);
            event -> accept ();
        }

    }

    else if (event -> button () == Qt::LeftButton) {

        QLabel *child = static_cast <QLabel*> (childAt (event -> pos ()));
        if (
                ! child || ! spellLabels.contains (child) || ! child -> pixmap () || child -> pixmap () -> isNull () ||
                child -> graphicsEffect () -> isEnabled ()
        ) {
            event -> ignore ();
            return;
        }

        QPixmap pixmap = *child -> pixmap ();
        pixmap = pixmap.scaled (32, 32, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);

        QByteArray itemData;
        QDataStream dataStream (&itemData, QIODevice::WriteOnly);
        dataStream << (qint8) spellLabels.indexOf (child);

        QMimeData *mimeData = new QMimeData;
        mimeData -> setData ("application/x-rospellbardata", itemData);

        QDrag *drag = new QDrag (this);
        drag -> setMimeData (mimeData);
        drag -> setPixmap (pixmap);
        drag -> setHotSpot (child -> mapFromGlobal (event -> globalPos ()));

        drag -> exec (Qt::MoveAction);

    }

    else {

        event -> ignore ();

    }

}

// IMPORTANT: encoded() and decode() assume that there are no more than seven spell trees and ten spells per tree, if
// this should change the methods will no longer work as expected!

QString SpellBar::encoded () {

    // returns an encoded string representing the current spellbar layout. there is no class information included in
    // this string! note that spells that are disabled will not be included!

    QString chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890<>[](){}";
    QString empty = ".";
    QString code  = "";

    for (int i = 0; i < spellData.size (); i++) {
        int t = spellData.at (i).first;
        int s = spellData.at (i).second;
        if (t == -1 || s == -1 || spellLabels.at (i) -> graphicsEffect () -> isEnabled ()) code += empty;
        else code += chars.at (t * 10 + s);
    }

    return code;

}

void SpellBar::decode (QString code) {

    // loads the spellbar setup specified by <code>.

    QString chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890<>[](){}";
    QChar empty = '.';

    // check code length:
    if (code.length () != NUM_BARS * NUM_SLOTS) return;

    setBar (0);

    for (int i = 0; i < NUM_BARS * NUM_SLOTS; i++) {

        // set current spell bar according to the position in the string:
        if (i % NUM_SLOTS == 0 && i > 0) setBar (bar + 1);

        // check for invalid chars:
        if (! (chars.contains (code.at (i)) || code.at (i) == '.')) return;

        // slot is empty:
        if (code.at (i) == empty) {
            spellData.replace (i, QPair <int, int> (-1, -1));
        }

        // slot contains a spell:
        else {
            int s = chars.indexOf (code.at (i)) % 10;
            int t = (chars.indexOf (code.at (i)) - s) / 10;
            spellData.replace (i, QPair <int, int> (t, s));
        }

        updateLabel (i % NUM_SLOTS);

    }

    setBar (0);

}
