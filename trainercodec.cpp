/*

Copyright 2011, 2014 Stefan Goebel <roskilltrainer -at- subtype.de> - Homepage: <http://subtype.de/>

This file is part of ROSkillTrainer.

ROSkillTrainer is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
version.

ROSkillTrainer is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with ROSkillTrainer. If not, see
<http://www.gnu.org/licenses/>.

*/

// encoding/decoding trainer setups to/from a single string. the resulting string's format is basically compatible to
// the format used on <http://trainer.claninquisition.org/trainer.html>, with the exception that we allow even tree
// levels (the trainer at claninquisition.org does load such a setup, but does not handle it correctly). in order to
// get a fully compatible encoded string use getCode(true), all even levels will be set to the next lower odd one in
// that case. note that in order to be compatible we do also set the first character of the encoded string to the
// encoded game version number, though different game versions are not supported by this application.

#include "trainercodec.h"

TrainerCodec::TrainerCodec (QString encoded) {

    // use this constructor to decode the specified string.

    tc          = TrainerConfig::config ();
    encodeChars = ENCODE_CHARS;
    code        = encoded;
    codeCompat  = "";
    className   = "";
    level       = -1;
    treeLevels  = QVector <int> ();
    spellLevels = QVector <QVector <int> > ();
    valid       = false;

    decode ();
    encodeCompat ();

}

void TrainerCodec::decode () {

    // decode the encoded string. if there is an error (ie. the code is invalid) the default values (see above) will be
    // kept.

    QString tCode = code;

    // simple regexp check first, check a minimal length to prevent errors during the remaining checks:
    QRegExp regexp ("[" + encodeChars + "]{3,}");
    if (! regexp.exactMatch (tCode)) return;

    // check and set class name:
    int classIndex = encodeChars.indexOf (tCode.at (1));
    QStringList classes = tc -> classStringList (false);
    if (classIndex >= classes.size ()) return;
    QString tClassName = classes.at (classIndex);

    // check and set character level:
    int tLevel = encodeChars.indexOf (tCode.at (2)) + 1;
    QPair <int, int> limits = tc -> classLimits (tClassName);
    if (tLevel < limits.first || tLevel > limits.second) return;

    // check code length:
    if (tCode.length () != tc -> classTrees (tClassName).size () * 6 + 3) return;

    // remove first three characters:
    tCode = tCode.mid (3);

    int maxTreeLevel = tc -> treeLevel (tLevel);

    QVector <int> tTreeLevels;
    QVector <QVector <int> > tSpellLevels;

    while (! tCode.isEmpty ()) {

        // check tree level:
        int treeLevel = encodeChars.indexOf (tCode.at (0));
        if (treeLevel > maxTreeLevel || treeLevel < 1) return;
        tTreeLevels.append (treeLevel);

        int maxSpellLevel = tc -> spellLevel (treeLevel);
        int maxIndex = ((treeLevel + (treeLevel % 2)) / 2) - 1;

        QVector <int> spells;

        for (int i = 1; i <= 5; i++) {

            QChar spellC = tCode.at (i);
            int spellLevel2 = encodeChars.indexOf (spellC) % 6;
            int spellLevel1 = (encodeChars.indexOf (spellC) - spellLevel2) / 6;

            // check if spell level is above the limit set by the tree level:
            if (spellLevel1 > maxSpellLevel || spellLevel2 > maxSpellLevel) return;

            // check if there is a spell level above 0 for disabled spells:
            if (((i * 2) - 2 > maxIndex && spellLevel1 > 0) || ((i * 2) - 1 > maxIndex && spellLevel2 > 0)) return;

            spells.append (spellLevel1);
            spells.append (spellLevel2);

        }

        tSpellLevels.append (spells);

        tCode = tCode.mid (6);

    }

    // passed all checks, set all variables accordingly:

    className   = tClassName;
    level       = tLevel;
    treeLevels  = tTreeLevels;
    spellLevels = tSpellLevels;
    valid       = true;

}

TrainerCodec::TrainerCodec (QString clsName, int charLvl, QVector <int> treeLvl, QVector <QVector <int> > spellLvl) {

    // use this constructor to generate the encoded string(s) for an existing setup. the setup has to be valid!

    tc          = TrainerConfig::config ();
    encodeChars = ENCODE_CHARS;
    code        = "";
    codeCompat  = "";
    className   = clsName;
    level       = charLvl;
    treeLevels  = treeLvl;
    spellLevels = spellLvl;
    valid       = false;

    encode ();

}

void TrainerCodec::encode () {

    // calculate the encoded string (both normal and compatible mode). if anything goes wrong, the encoded strings will
    // be empty. every input data is checked for validity. it is still possible to encode setups with a negative number
    // of available discipline or power points, apart from this the encoded string (if any) is guaranteed to represent
    // a valid setup.

    // check class name:
    int classIndex = tc -> classIndex (className);
    if (classIndex == -1) return;

    // check level:
    QPair <int, int> limits = tc -> classLimits (className);
    if (level < limits.first || level > limits.second) return;

    // check the number of trees and spell level lists:
    if (treeLevels.size () != tc -> classTrees (className).size ()) return;
    if (treeLevels.size () != spellLevels.size ()) return;

    QString tCode, tCodeCompat;

    tCode = QString (VERSION_CODE + QString (encodeChars.at (classIndex)) + QString (encodeChars.at (level - 1)));
    tCodeCompat = tCode;

    int maxTreeLevel = tc -> treeLevel (level);
    int spellNumber = tc -> spellNumber ();

    for (int i = 0; i < treeLevels.size (); i++) {

        // check tree level:
        int treeLevel = treeLevels.at (i);
        if (treeLevel > maxTreeLevel || treeLevel < 0) return;

        tCode += encodeChars.at (treeLevel);
        int treeLevelCompat = treeLevel - (treeLevel + 1) % 2;
        tCodeCompat += encodeChars.at (treeLevelCompat);

        // check number of spells for the tree:
        int currentSpellNumber = spellLevels.at (i).size ();
        if (currentSpellNumber > spellNumber) return;

        int maxSpellLevel = tc -> spellLevel (treeLevel);
        int maxIndex = ((treeLevel + (treeLevel % 2)) / 2) - 1;

        for (int j = 1; j <= currentSpellNumber; j += 2) {

            int spellLevel1 = spellLevels.at (i).at (j - 1);
            int spellLevel2 = spellLevels.at (i).at (j);

            // check if a spell level is out of the valid range:
            if (spellLevel1 > maxSpellLevel || spellLevel2 > maxSpellLevel || spellLevel1 < 0 || spellLevel2 < 0)
                return;

            // check if a disabled spell has a level above 0:
            if (((j - 1) > maxIndex && spellLevel1 > 0) || (j > maxIndex && spellLevel2 > 0)) return;

            QChar c = encodeChars.at (spellLevel1 * 6 + spellLevel2);
            tCode       += c;
            tCodeCompat += c;

        }

    }

    code       = tCode;
    codeCompat = tCodeCompat;
    valid      = true;

}

void TrainerCodec::encodeCompat () {

    // this method will generate the encoded string in compatible mode, basically the same as above, but without any
    // checks. this is meant to be called from TrainerConfig(QString) after decode() was called and the specified code
    // has already been checked for validity. if valid is set to false this method will do nothing.

    if (! valid) return;

    codeCompat = QString (
            VERSION_CODE +
            QString (encodeChars.at (tc -> classIndex (className))) +
            QString (encodeChars.at (level - 1))
        );

    for (int i = 0; i < treeLevels.size (); i++) {

        int lvl = treeLevels.at (i);
        codeCompat += encodeChars.at (lvl - (lvl + 1) % 2);

        for (int j = 1; j <= spellLevels.at (i).size (); j += 2)
            codeCompat += encodeChars.at (spellLevels.at (i).at (j - 1) * 6 + spellLevels.at (i).at (j));

    }

}

// accessor methods (setting new values is not supported, a new instance has to be created if required):

QString                  TrainerCodec::getCode        (bool compat) const { return compat ? codeCompat : code; }
QString                  TrainerCodec::getClass       ()            const { return className;                  }
int                      TrainerCodec::getLevel       ()            const { return level;                      }
QVector <int>            TrainerCodec::getTreeLevels  ()            const { return treeLevels;                 }
QVector <QVector <int> > TrainerCodec::getSpellLevels ()            const { return spellLevels;                }
bool                     TrainerCodec::isValid        ()            const { return valid;                      }

QString TrainerCodec::getEncodeChars () {
    // static method that returns the valid encoding characters.
    return ENCODE_CHARS;
}
