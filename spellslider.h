/*

Copyright 2011, 2014 Stefan Goebel <roskilltrainer -at- subtype.de> - Homepage: <http://subtype.de/>

This file is part of ROSkillTrainer.

ROSkillTrainer is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
version.

ROSkillTrainer is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with ROSkillTrainer. If not, see
<http://www.gnu.org/licenses/>.

*/

#ifndef SPELLSLIDER_H
#define SPELLSLIDER_H

#include "levelslider.h"
#include "pointspool.h"
#include "trainerconfig.h"

#include <QMouseEvent>

class SpellSlider: public LevelSlider {

    Q_OBJECT

    public:

        explicit SpellSlider (int tNum = -1, int sNum = -1, QWidget *parent = 0);
        void setSpell (QString icon, QString tooltip, bool isPassive);
        void reset ();

    signals:

        void spellStatus (bool enabled, int tNum, int sNum);

    private:

        PointsPool *pp;
        TrainerConfig *tc;
        int origVal, treeNum, spellNum;
        bool passive;

    private slots:

        void updateMaximum (int avail);
        void changePoints (int newValue);
        void emitSignals (int newValue);

    protected:

        void mousePressEvent (QMouseEvent *event);

};

#endif // SPELLSLIDER_H
