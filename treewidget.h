/*

Copyright 2011, 2014 Stefan Goebel <roskilltrainer -at- subtype.de> - Homepage: <http://subtype.de/>

This file is part of ROSkillTrainer.

ROSkillTrainer is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
version.

ROSkillTrainer is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with ROSkillTrainer. If not, see
<http://www.gnu.org/licenses/>.

*/

#ifndef TREEWIDGET_H
#define TREEWIDGET_H

#include "spellslider.h"
#include "trainerconfig.h"
#include "transparentslider.h"
#include "treeslider.h"

#include <QGroupBox>

class TreeWidget: public QGroupBox {

    Q_OBJECT

    public:

        explicit TreeWidget(int tNum = -1, QWidget *parent = 0);

        void setTree (QString treeDir);
        void setMaximum (int level);
        void reset ();
        void resetPowers ();
        void setLevel (int level);
        int getLevel () const;
        QVector <int> getSpellLevels () const;
        void setSpellLevels (QVector <int> levels);

    signals:

        void spellStatus (bool enabled, int tNum, int sNum);

    public slots:

        void setShowSlider (bool enabled);
        void setButtonsEnabled (bool enabled);

    private:

        QVector <SpellSlider*> spells;
        TreeSlider *tree;
        PointsPool *pp;
        TransparentSlider *slider;
        TrainerConfig *tc;
        int treeNum;

    private slots:

        void checkSliderMax ();
        void enableSpells ();
        void updateSliderToolTip ();
        void emitSignals (bool enabled, int tNum, int sNum);

};

#endif // TREEWIDGET_H
