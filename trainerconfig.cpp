/*

Copyright 2011, 2014 Stefan Goebel <roskilltrainer -at- subtype.de> - Homepage: <http://subtype.de/>

This file is part of ROSkillTrainer.

ROSkillTrainer is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
version.

ROSkillTrainer is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with ROSkillTrainer. If not, see
<http://www.gnu.org/licenses/>.

*/

#include "trainerconfig.h"

#include <QFile>
#include <QTextStream>

TrainerConfig *TrainerConfig::instance = 0;

TrainerConfig* TrainerConfig::config (QObject *parent) {
    if (! instance) instance = new TrainerConfig (parent);
    return instance;
}

TrainerConfig::TrainerConfig (QObject *parent): QObject (parent) {
    loadClasses ("classes." + tr ("en"));
    disciplineCosts = readSingleValues ("discipline_costs");
    maxSpellLevels  = readSingleValues ("max_spell_levels");
    maxTreeLevels   = readSingleValues ("max_tree_levels");
}

bool TrainerConfig::classExists (QString className, Qt::CaseSensitivity cs) const {
    // returns true if the specified class exists. by default search is done case sensitively, use the second parameter
    // to change that if required.
    return classOrder.contains (classNameClean (className), cs);
}

QString TrainerConfig::classNameClean (QString className) const {
    // returns the specified class name with leading and trailing spaces stripped.
    return className.remove (QRegExp ("^\\s*")).remove (QRegExp ("\\s*$"));
}

QVector <QPair <QString, bool> > TrainerConfig::classList () const {

    // returns a list of classes in the configured order. first element of the QPair is the class name, second element
    // is true if the class is a subclass, else false.

    QVector <QPair <QString, bool> > result;
    foreach (QString name, classOrder) result.append (QPair <QString, bool> (name, classInfo.value (name).isSubClass));
    return result;

}

QStringList TrainerConfig::classStringList (bool indent) const {

    // returns a list of classes in the configured order, as a string list, subclasses will have three spaces prepended
    // to the class name if <indent> is true (default).

    QStringList result;
    foreach (QString name, classOrder)
        result.append ((classInfo.value (name).isSubClass && indent) ? "   " + name : name);
    return result;

}

int TrainerConfig::classIndex (QString className) const {
    // returns the (zero based) position of the specified class in the list based on the configured order. returns -1
    // if the class does not exist.
    return classOrder.indexOf (classNameClean (className));
}

int TrainerConfig::minTreeNumber () const {
    // returns the minimum number of trees of any class.
    int min = -1;
    QMapIterator <QString, tClassInfo> iter (classInfo);
    while (iter.hasNext ()) {
        iter.next ();
        int spells = iter.value ().spells.size ();
        if (spells < min || min == -1) min = spells;
    }
    return min;
}

int TrainerConfig::maxTreeNumber () const {
    // returns the maximum number of trees of any class.
    int max = -1;
    QMapIterator <QString, tClassInfo> iter (classInfo);
    while (iter.hasNext ()) {
        iter.next ();
        int spells = iter.value ().spells.size ();
        if (spells > max) max = spells;
    }
    return max;
}

int TrainerConfig::spellNumber () const {
    // returns the number of spells in a spell tree. this assumes that a spell is enabled every two tree levels.
    int size = maxSpellLevels.size ();
    return (size + (size % 2)) / 2;
}

int TrainerConfig::maxTreeLevel () const {
    //returns the maximum level of a spell tree:
    return maxSpellLevels.size ();
}

QPair <int, int> TrainerConfig::classLimits (QString className) const {
    // returns the minimum and maximum level for a specified class (in that order). returns <-1, -1> if the class does
    // not exist.
    className = classNameClean (className);
    if (! classExists (className)) return QPair <int, int> (-1, -1);
    return QPair <int, int> (classInfo.value (className).minLevel, classInfo.value (className).maxLevel);
}

QStringList TrainerConfig::classTrees (QString className) const {
    // returns the spell trees for the specified class, an empty list if the class does not exist.
    className = classNameClean (className);
    if (! classExists (className)) return QStringList ();
    return classInfo.value (className).spells;
}

int TrainerConfig::classDP (QString className, int level) const {
    // returns the total number of discipline points available to specified class at the specified level. returns -1 if
    // the class does not exist.
    className = classNameClean (className);
    if (! classExists (className)) return -1;
    int indexOffset = classInfo.value (className).minLevel;
    return classInfo.value (className).disciplinePoints -> at (level - indexOffset);
}

int TrainerConfig::classPP (QString className, int level) const {
    // returns the total number of power points available to specified class at the specified level. returns -1 if the
    // class does not exist.
    className = classNameClean (className);
    if (! classExists (className)) return -1;
    int indexOffset = classInfo.value (className).minLevel;
    return classInfo.value (className).powerPoints -> at (level - indexOffset);
}

int TrainerConfig::dpCost (int from, int to) const {

    // returns the number of discipline points required to get from level <from> to <to>, the result may be negative if
    // <from> is greater than <to>. if <to> is -1 (default value) it is assumed to be <from> + 1. returns 255 if the
    // level limits are exceeded.

    bool sign = false;
    int cost = 0;

    if (to == -1) to = from + 1;

    if (from > to) {
        sign = true;
        int temp = from;
        from = to;
        to = temp;
    }
    else if (from == disciplineCosts.size () + 1) return 255;

    for (int i = from - 1; i < to - 1; i++) cost += disciplineCosts.at (i);

    return sign ? - cost : cost;

}

// need to check why this doesn't work with const...
int TrainerConfig::spellLevel (int level) {
    // returns the maximum spell level for the specified tree level <level>.
    return maxSpellLevels.at (level - 1);
}

int TrainerConfig::treeLevel (int level) const {
    // returns the maximum tree level for the specified character level <level>.
    return maxTreeLevels.at (level - 1);
}

int TrainerConfig::numSubClasses (QString className) const {
    // returns the number of subclasses of the specified. returns -1 if the class does not exist.
    className = classNameClean (className);
    if (! classExists (className)) return -1;
    if (subClasses.contains (className)) return subClasses.value (className).size ();
    else return 0;
}

void TrainerConfig::loadClasses (QString fName) {

    // file format of the classes file: \s*<class name>\t+<spell trees>\t+<point config>\n
    // if there are leading spaces the class is considered to be a subclass. the spell tree names must be separated by
    // spaces.

    QFile file (":/config/" + fName);
    if (! file.open (QIODevice::ReadOnly | QIODevice::Text)) return;

    QTextStream str (&file);
    QString line = str.readLine ();

    QMap <QString, QString> pointConfig;

    QString mainClass;

    while (! line.isNull ()) {

        if (line.isEmpty ()) {
            line = str.readLine ();
            continue;
        }

        QStringList fields = line.split ("\t", QString::SkipEmptyParts);

        QString className = fields.at (0);
        QString pointFile = fields.at (2);

        bool isSubClass = className.startsWith (" ");
        className.remove (QRegExp ("^\\s+"));

        if (isSubClass) subClasses [mainClass].append (className);
        else mainClass = className;

        classOrder.append (className);

        classInfo [className].isSubClass = isSubClass;
        classInfo [className].spells = fields.at (1).split (" ", QString::SkipEmptyParts);

        if (pointConfig.contains (pointFile)) {

            // if the specific points file has been read already, simply copy the stuff from the other class:
            classInfo [className].disciplinePoints = classInfo.value (pointConfig.value (pointFile)).disciplinePoints;
            classInfo [className].powerPoints      = classInfo.value (pointConfig.value (pointFile)).powerPoints;
            classInfo [className].minLevel         = classInfo.value (pointConfig.value (pointFile)).minLevel;
            classInfo [className].maxLevel         = classInfo.value (pointConfig.value (pointFile)).maxLevel;

        }
        else {

            // read the points file:
            tPointsInfo points = readPointValues (pointFile);

            // and set the values:
            classInfo [className].disciplinePoints = points.disciplinePoints;
            classInfo [className].powerPoints      = points.powerPoints;
            classInfo [className].minLevel         = points.minLevel;
            classInfo [className].maxLevel         = points.maxLevel;

            // and remember that we already read this file:
            pointConfig [pointFile] = className;

        }

        line = str.readLine ();

    }

    file.close ();

}

TrainerConfig::tPointsInfo TrainerConfig::readPointValues (QString fName) {

    // file format of the points file: "<level> <discipline points> <power points>". the <level> is sctually ignored,
    // make sure the stuff is in the correct order!

    tPointsInfo info;

    info.disciplinePoints = new QVector <int>;
    info.powerPoints      = new QVector <int>;
    info.minLevel         = -1;
    info.maxLevel         = -1;

    QFile file (":/config/" + fName);
    if (! file.open (QIODevice::ReadOnly | QIODevice::Text)) return info;

    QTextStream str (&file);
    QString line = str.readLine ();

    while (! line.isNull ()) {

        if (line.isEmpty ()) {
            line = str.readLine ();
            continue;
        }

        QStringList fields = line.split (" ", QString::SkipEmptyParts);

        info.maxLevel = fields.at (0).toInt ();
        if (info.minLevel == -1) info.minLevel = info.maxLevel;

        info.disciplinePoints -> append (fields.at (1).toInt ());
        info.powerPoints      -> append (fields.at (2).toInt ());

        line = str.readLine ();

    }

    file.close ();

    return info;

}

QVector <int> TrainerConfig::readSingleValues (QString fName) {

    // file format of these files: "<level> <points>". the <level> is actually ignored, make sure the stuff is in the
    // correct order!

    QVector <int> values;

    QFile file (":/config/" + fName);
    if (! file.open (QIODevice::ReadOnly | QIODevice::Text)) return values;

    QTextStream str (&file);
    QString line = str.readLine ();

    while (! line.isNull ()) {
        if (line.isEmpty ()) {
            line = str.readLine ();
            continue;
        }
        QStringList fields = line.split (" ", QString::SkipEmptyParts);
        values.append (fields.at (1).toInt ());
        line = str.readLine ();
    }

    file.close ();

    return values;

}
