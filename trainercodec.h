/*

Copyright 2011, 2014 Stefan Goebel <roskilltrainer -at- subtype.de> - Homepage: <http://subtype.de/>

This file is part of ROSkillTrainer.

ROSkillTrainer is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
version.

ROSkillTrainer is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with ROSkillTrainer. If not, see
<http://www.gnu.org/licenses/>.

*/

#ifndef TRAINERCODEC_H
#define TRAINERCODEC_H

#include "trainerconfig.h"

#include <QString>
#include <QVector>

#define ENCODE_CHARS "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789"
#define VERSION_CODE "d"

class TrainerCodec {

    public:

        TrainerCodec (QString encoded);
        TrainerCodec (QString clsName, int charLvl, QVector <int> treeLvl, QVector <QVector <int> > spellLvl);

        bool isValid () const;
        QString getCode (bool compat = false) const;
        QString getClass () const;
        int getLevel () const;
        QVector <int> getTreeLevels () const;
        QVector <QVector <int> > getSpellLevels () const;

        static QString getEncodeChars ();

    private:

        QString code, codeCompat, className, encodeChars;
        int level;
        QVector <int> treeLevels;
        QVector <QVector <int> > spellLevels;
        TrainerConfig *tc;
        bool valid;

        void encode ();
        void encodeCompat ();
        void decode ();

};

#endif // TRAINERCODEC_H
