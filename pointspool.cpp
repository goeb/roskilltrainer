/*

Copyright 2011, 2014 Stefan Goebel <roskilltrainer -at- subtype.de> - Homepage: <http://subtype.de/>

This file is part of ROSkillTrainer.

ROSkillTrainer is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
version.

ROSkillTrainer is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with ROSkillTrainer. If not, see
<http://www.gnu.org/licenses/>.

*/

#include "pointspool.h"

PointsPool *PointsPool::instance = 0;

PointsPool::PointsPool (QObject *parent): QObject (parent) {
    dpUsed  = 0;
    dpTotal = 0;
    ppUsed  = 0;
    ppTotal = 0;
}

PointsPool* PointsPool::pool (QObject *parent) {
    if (! instance) instance = new PointsPool (parent);
    return instance;
}

int PointsPool::getDPTotal () const { return dpTotal; }
int PointsPool::getPPTotal () const { return ppTotal; }

int PointsPool::getDPUsed () const { return dpUsed; }
int PointsPool::getPPUsed () const { return ppUsed; }

int PointsPool::getDPAvailable () const { return dpTotal - dpUsed; }
int PointsPool::getPPAvailable () const { return ppTotal - ppUsed; }

void PointsPool::setDPTotal (int total) {
    if (total < 0) return;
    if (dpTotal != total) {
        dpTotal = total;
        emit dpTotalChanged (dpTotal);
        emit dpAvailableChanged (getDPAvailable ());
    }
}

void PointsPool::setDPUsed (int used) {
    if (used < 0) return;
    if (dpUsed != used) {
        dpUsed = used;
        emit dpAvailableChanged (getDPAvailable ());
    }
}

void PointsPool::setPPTotal (int total) {
    if (total < 0) return;
    if (ppTotal != total) {
        ppTotal = total;
        emit ppTotalChanged (ppTotal);
        emit ppAvailableChanged (getPPAvailable ());
    }
}

void PointsPool::setPPUsed (int used) {
    if (used < 0) return;
    if (ppUsed != used) {
        ppUsed = used;
        emit ppAvailableChanged (getPPAvailable ());
    }
}

void PointsPool::incDPUsed (int value) { setDPUsed (dpUsed + value); }
void PointsPool::decDPUsed (int value) { setDPUsed (dpUsed - value); }
void PointsPool::incPPUsed (int value) { setPPUsed (ppUsed + value); }
void PointsPool::decPPUsed (int value) { setPPUsed (ppUsed - value); }
